/*
	File: fn_spawnVehicle.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Sends the query request to the database, if an array is returned then it creates
	the vehicle if it's not in use or dead.
*/
private["_vid","_sp","_pid","_query","_sql","_vehicle","_nearVehicles","_name","_side","_tickTime","_dir"];
_vid = [_this,0,-1,[0]] call BIS_fnc_param;
_pid = [_this,1,"",[""]] call BIS_fnc_param;
_sp = [_this,2,[],[[],""]] call BIS_fnc_param;
_unit = [_this,3,ObjNull,[ObjNull]] call BIS_fnc_param;
_price = [_this,4,0,[0]] call BIS_fnc_param;
_dir = [_this,5,0,[0]] call BIS_fnc_param;
_unit_return = _unit;
_name = name _unit;
_side = side _unit;
_unit = owner _unit;

if(_vid == -1 OR _pid == "") exitWith {};
if(_vid in serv_sv_use) exitWith {};
serv_sv_use pushBack _vid;

_query = format["SELECT id, side, classname, type, pid, alive, active, plate, color, assur FROM vehicles WHERE id='%1' AND pid='%2'",_vid,_pid];

waitUntil{sleep (random 0.3); !DB_Async_Active};
_tickTime = diag_tickTime;
_queryResult = [_query,2] call DB_fnc_asyncCall;

diag_log "------------- Client Query Request -------------";
diag_log format["QUERY: %1",_query];
diag_log format["Time to complete: %1 (in seconds)",(diag_tickTime - _tickTime)];
diag_log format["Result: %1",_queryResult];
diag_log "------------------------------------------------";

if(typeName _queryResult == "STRING") exitWith {};

_vInfo = _queryResult;
if(isNil "_vInfo") exitWith {serv_sv_use = serv_sv_use - [_vid];};
if(count _vInfo == 0) exitWith {serv_sv_use = serv_sv_use - [_vid];};

if((_vInfo select 5) == 0) exitWith
{
	serv_sv_use = serv_sv_use - [_vid];
	[[1,format["Désolé mais votre %1 est répertorié comme détruit.",_vInfo select 2]],"life_fnc_broadcast",_unit,false] spawn life_fnc_MP;
};

if((_vInfo select 6) == 1) exitWith
{
	serv_sv_use = serv_sv_use - [_vid];
	[[1,format["Désolé mais votre %1 est déja actif.",_vInfo select 2]],"life_fnc_broadcast",_unit,false] spawn life_fnc_MP;
};

if(typeName _sp != "STRING") then {
    _nearVehicles = nearestObjects[_sp,["Car","Air","Ship"],10];
} else {
    _nearVehicles = [];
};
if(count _nearVehicles > 0) exitWith
{
    serv_sv_use = serv_sv_use - [_vid];
    [[_price,_unit_return],"life_fnc_garageRefund",_unit,false] spawn life_fnc_MP;
   	[[1,format["Impossible de faire apparaitre votre véhicule."]],"life_fnc_broadcast",_unit,false] spawn life_fnc_MP;
};


_query = format["UPDATE vehicles SET active='1' WHERE pid='%1' AND id='%2'",_pid,_vid];

waitUntil {!DB_Async_Active};
[_query,false] spawn DB_fnc_asyncCall;

if(typeName _sp == "STRING") then {
	_vehicle = createVehicle[(_vInfo select 2),[0,0,999],[],0,"NONE"];
	waitUntil {!isNil "_vehicle" && {!isNull _vehicle}};
	_vehicle allowDamage false;
	_hs = nearestObjects[getMarkerPos _sp,["Land_Hospital_side2_F"],50] select 0;
    _vehicle setPosATL (_hs modelToWorld [-0.4,-4,12.65]);
	sleep 0.6;
} else {
	_vehicle = createVehicle [(_vInfo select 2),_sp,[],0,"NONE"];
	waitUntil {!isNil "_vehicle" && {!isNull _vehicle}};
	_vehicle allowDamage false;
	_vehicle setPos _sp;
	_vehicle setVectorUp (surfaceNormal _sp);
	_vehicle setDir _dir;
};
_vehicle allowDamage true;
//Send keys over the network.
[[_vehicle],"life_fnc_addVehicle2Chain",_unit,false] spawn life_fnc_MP;
[_pid,_side,_vehicle,1] call TON_fnc_keyManagement;
_vehicle lock 2;
//Reskin the vehicle 
[[_vehicle,_vInfo select 8],"life_fnc_colorVehicle",nil,false] spawn life_fnc_MP;
_vehicle setVariable["vehicle_info_owners",[[_pid,_name]],true];
_vehicle setVariable["vehicle_info_tracker",[],true];
_vehicle setVariable["vehicle_info_c4",[],true];
_vehicle setVariable["dbInfo",[(_vInfo select 4),_vInfo select 7]];
//_vehicle addEventHandler["Killed","_this spawn TON_fnc_vehicleDead"]; //Obsolete function?
[_vehicle] call life_fnc_clearVehicleAmmo;
//_vehicle addEventHandler ["GetIn",{_this spawn life_fnc_getIn;}];

//Assurance
if((_vInfo select 9) == 1) then
{
	_vehicle setVariable["dbAssur",true,true];
	diag_log format["ASSUR TRUE"];
}
else
{
	_vehicle setVariable["dbAssur",false,true];
		diag_log format["ASSUR FALSE"];
};

//Sets of animations
if((_vInfo select 1) == "civ" && (_vInfo select 2) == "B_Heli_Light_01_F" && _vInfo select 8 != 13 && _vInfo select 8 != 15) then
{
	[[_vehicle,"civ_littlebird",true],"life_fnc_vehicleAnimate",_unit,false] spawn life_fnc_MP;
		diag_log format["HM9"];
};
//Camion Pegasus
if((_vInfo select 1) == "civ" && (_vInfo select 2) == "I_Truck_02_box_F" && _vInfo select 8 == 0) then
{

	_vehicle setRepairCargo 0;
	diag_log format["PEGASUS"];

};


//Medic
if((_vInfo select 1) == "civ" && (_vInfo select 2) == "C_SUV_01_F" && _vInfo select 8 == 7) then {

_vehicle setVariable["lightsMedic",false,true];
_vehicle setVariable["SirenMedic",false,true];
};
if((_vInfo select 1) == "civ" && (_vInfo select 2) == "O_Heli_Light_02_unarmed_F" && _vInfo select 8 == 4) then {

_vehicle setVariable["lightsMedic",false,true];
_vehicle setVariable["SirenMedic",false,true];
};
if((_vInfo select 1) == "civ" && (_vInfo select 2) == "B_Heli_Light_01_F" && _vInfo select 8 == 14) then {

_vehicle setVariable["lightsMedic",false,true];
_vehicle setVariable["SirenMedic",false,true];
};
if((_vInfo select 1) == "civ" && (_vInfo select 2) == "C_Hatchback_01_sport_F" && _vInfo select 8 == 11) then {

_vehicle setVariable["lightsMedic",false,true];
_vehicle setVariable["SirenMedic",false,true];
};
if((_vInfo select 1) == "civ" && (_vInfo select 2) == "C_Hatchback_01_F" && _vInfo select 8 == 9) then {

_vehicle setVariable["lightsMedic",false,true];
_vehicle setVariable["SirenMedic",false,true];
};


//Depanneur
if((_vInfo select 1) == "civ" && (_vInfo select 2) == "C_Offroad_01_F" && _vInfo select 8 == 9) then
{
	[[_vehicle,"service_truck",true],"life_fnc_vehicleAnimate",_unit,false] spawn life_fnc_MP;
			diag_log format["DEPANNEUR"];
};

if((_vInfo select 1) == "cop" && (_vInfo select 2) in ["C_Offroad_01_F","B_MRAP_01_F","C_SUV_01_F"]) then
{
	[[_vehicle,"cop_offroad",true],"life_fnc_vehicleAnimate",_unit,false] spawn life_fnc_MP;
};
 
//SCRIPT ANTI BLOOD
if((_vInfo select 2) in ["I_Plane_Fighter_03_AA_F","O_Heli_Light_02_v2_F","B_MBT_01_mlrs_F","B_Heli_Transport_03_F","I_Heli_light_03_F","O_Heli_Light_02_F","B_Heli_Transport_01_camo_F","I_APC_Wheeled_03_cannon_F","B_UAV_02_F","B_Heli_Light_01_armed_F","B_MRAP_01_hmg_F","B_UAV_02_CAS_F","O_APC_Wheeled_02_rcws_F","O_UGV_01_rcws_F","O_MBT_02_arty_F","O_MBT_02_cannon_F","O_APC_Tracked_02_AA_F","O_APC_Tracked_02_cannon_F","O_Heli_Attack_02_F","O_Plane_CAS_02_F","B_UGV_01_rcws_F","B_MBT_01_TUSK_F","B_MBT_01_arty_F","B_MBT_01_cannon_F","B_APC_Tracked_01_AA_F","B_APC_Tracked_01_rcws_F","I_MRAP_03_gmg_F","I_Plane_Fighter_03_CAS_F","B_Plane_CAS_01_F","O_Heli_Attack_02_black_F","I_MBT_03_cannon_F","I_MRAP_03_hmg_F","I_APC_tracked_03_cannon_F","I_Plane_Fighter_03_AA_F","B_APC_Wheeled_01_cannon_F"]) then {
	
deleteVehicle _vehicle;
_query = format["UPDATE vehicles SET alive='0' WHERE pid='%1' AND id='%2'",_pid,_vid];
waitUntil {!DB_Async_Active};
[_query,false] spawn DB_fnc_asyncCall;
};

//SCRIPT ANTI BLOOD
if(_pid == "76561198057953365" && (_vInfo select 2) in ["O_Heli_Attack_02_black_F"]) then {
deleteVehicle _vehicle;
_query = format["UPDATE vehicles SET alive='0' WHERE pid='%1' AND id='%2'",_pid,_vid];
waitUntil {!DB_Async_Active};
[_query,false] spawn DB_fnc_asyncCall;
};


//Script Anti Oldveh
if((_vInfo select 1) == "civ" && (_vInfo select 2) in ["I_MRAP_03_F"] && _vInfo select 8 != 1) then {
deleteVehicle _vehicle;
_query = format["UPDATE vehicles SET alive='0' WHERE pid='%1' AND id='%2'",_pid,_vid];
waitUntil {!DB_Async_Active};
[_query,false] spawn DB_fnc_asyncCall;
};

if((_vInfo select 1) == "cop" && (_vInfo select 2) in ["C_Hatchback_01_F","C_Hatchback_01_sport_F","C_Offroad_01_F","B_G_Offroad_01_armed_F","C_SUV_01_F","B_Heli_Light_01_F","B_Heli_Transport_01_F","I_Heli_light_03_unarmed_F","I_MRAP_03_hmg_F","I_MRAP_03_F","B_APC_Wheeled_01_cannon_F","B_MRAP_01_hmg_F","B_MRAP_01_F","B_Quadbike_01_F"]) then {

_vehicle setVariable["lights",false,true];
_vehicle setVariable["SirenCop",false,true];
};
/*
if((_vInfo select 1) == "med" && (_vInfo select 2) == "C_Offroad_01_F") then
{
	[[_vehicle,"med_offroad",true],"life_fnc_vehicleAnimate",_unit,false] spawn life_fnc_MP;
};
*/
//Script degat
[[_vehicle],"life_fnc_vehicleAfterSpawn",_unit,false] spawn life_fnc_MP;
[[1,"Votre véhicule est prêt !"],"life_fnc_broadcast",_unit,false] spawn life_fnc_MP;
	diag_log format["END SPAWNVEH"];
serv_sv_use = serv_sv_use - [_vid];
