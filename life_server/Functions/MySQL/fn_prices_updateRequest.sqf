/*
	File: fn_updateRequest.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Ain't got time to describe it, READ THE FILE NAME!
*/
private["_amount","_var"];
_amount = [_this,0,0,[0]] call BIS_fnc_param;
_var = [_this,1,"",[""]] call BIS_fnc_param;
diag_log format ["TOTAL VENDU %1 _var %2 _amount", _var, _amount];
//diag_log format ["Server update prices  Namitems %1 | _var %2 | _amount %3 | _price %4", _nameitems, _var, _amount, _price];
//_query = format["UPDATE bourses SET total_vendu=total_vendu+%2 WHERE '%1'",_var, _amount];
_query = format["UPDATE economy SET total_vendu = (total_vendu + %2) WHERE ressource = '%1'",_var, _amount];


waitUntil {!DB_Async_Active};
_thread = [_query,false] spawn DB_fnc_asyncCall;
waitUntil {scriptDone _thread};