life_fnc_sidechat =
compileFinal "
	if(life_sidechat) then {life_sidechat = false;} else {life_sidechat = true;};
	[[player,life_sidechat,playerSide],""TON_fnc_managesc"",false,false] spawn life_fnc_MP;
	[] call life_fnc_settingsMenu;
";

publicVariable "life_fnc_sidechat";

TON_fnc_index =
compileFinal "
	private[""_item"",""_stack""];
	_item = _this select 0;
	_stack = _this select 1;
	_return = -1;

	{
		if(_item in _x) exitWith {
			_return = _forEachIndex;
		};
	} foreach _stack;

	_return;
";
publicVariable "TON_fnc_index";

TON_fnc_player_query =
compileFinal "
	private[""_ret""];
	_ret = _this select 0;
	if(isNull _ret) exitWith {};
	if(isNil ""_ret"") exitWith {};
	
	[[life_dabflouze,life_flouze,owner player,player],""life_fnc_admininfo"",_ret,false] spawn life_fnc_MP;
";
publicVariable "TON_fnc_player_query";

TON_fnc_clientWireTransfer =
compileFinal "
	private[""_unit"",""_val"",""_from""];
	_val = _this select 0;
	_from = _this select 1;
	if(!([str(_val)] call TON_fnc_isnumber)) exitWith {};
	if(_from == """") exitWith {};
	life_dabflouze = life_dabflouze + _val;
	hint format[""%1 vous a transféré %2€."",_from,[_val] call life_fnc_numberText];
	
";
publicVariable "TON_fnc_clientWireTransfer";

TON_fnc_isnumber =
compileFinal "
	private[""_valid"",""_value"",""_compare""];
	_value = _this select 0;
	_valid = [""0"",""1"",""2"",""3"",""4"",""5"",""6"",""7"",""8"",""9""];
	_array = [_value] call KRON_StrToArray;
	_return = true;
	
	{
		if(_x in _valid) then	
		{}
		else
		{
			_return = false;
		};
	} foreach _array;
	_return;
";

publicVariable "TON_fnc_isnumber";

TON_fnc_clientGangKick =
compileFinal "
	private[""_unit"",""_group""];
	_unit = _this select 0;
	_group = _this select 1;
	if(isNil ""_unit"" OR isNil ""_group"") exitWith {};
	if(player == _unit && (group player) == _group) then
	{
		life_my_gang = ObjNull;
		[player] joinSilent (createGroup civilian);
		hint ""Vous avez été exclu du Gang !"";
		
	};
";
publicVariable "TON_fnc_clientGangKick";

TON_fnc_clientGetKey =
compileFinal "
	private[""_vehicle"",""_unit"",""_giver""];
	_vehicle = _this select 0;
	_unit = _this select 1;
	_giver = _this select 2;
	if(isNil ""_unit"" OR isNil ""_giver"") exitWith {};
	if(player == _unit && !(_vehicle in life_vehicles)) then
	{
		_name = getText(configFile >> ""CfgVehicles"" >> (typeOf _vehicle) >> ""displayName"");
		hint format[""Quelqu'un vous a donné les clefs d'un(e) %2"",_giver,_name];
		life_vehicles pushBack _vehicle;
        [[getPlayerUID player,playerSide,_vehicle,1],""TON_fnc_keyManagement"",false,false] spawn life_fnc_MP;


	};
";

publicVariable "TON_fnc_clientGetKey";

TON_fnc_clientGangLeader =
compileFinal "
	private[""_unit"",""_group""];
	_unit = _this select 0;
	_group = _this select 1;
	if(isNil ""_unit"" OR isNil ""_group"") exitWith {};
	if(player == _unit && (group player) == _group) then
	{
		player setRank ""COLONEL"";
		_group selectLeader _unit;
		hint ""Vous avez été élu leader du Gang!"";
	};
";

publicVariable "TON_fnc_clientGangLeader";

//Cell Phone Messaging
/*
	-fnc_cell_textmsg
	-fnc_cell_textcop
	-fnc_cell_textadmin
	-fnc_cell_adminmsg
	-fnc_cell_adminmsgall
*/
/*
//To EMS/
fnc_cell_emsrequest = 
compileFinal "
private[""_msg"",""_to""];
	ctrlShow[3022,false];
	_msg = ctrlText 3003;
	_to = ""EMS Units"";
	if(_msg == """") exitWith {hint ""Vous devez saisir un message à envoyer !"";ctrlShow[3022,true];};
		
	[[_msg,name player,5],""clientMessage"",independent,false] spawn life_fnc_MP;
	[] call life_fnc_cellphone;
	hint format[""Vous avez envoyé un message à tous les médecins !"",_to,_msg];
	ctrlShow[3022,true];
";
*/
//To One Person
fnc_cell_textmsg =
compileFinal "
	private[""_msg"",""_to""];
	ctrlShow[3015,false];
	_msg = ctrlText 1400;
	if(lbCurSel 3004 == -1) exitWith {hint ""Vous devez selectionner un destinataire pour envoyer un message !""; ctrlShow[3015,true];};
	_to = call compile format[""%1"",(lbData[3004,(lbCurSel 3004)])];
	if(isNull _to) exitWith {ctrlShow[3015,true];};
	if(isNil ""_to"") exitWith {ctrlShow[3015,true];};
	if(_msg == """") exitWith {hint ""Vous devez saisir un message à envoyer !"";ctrlShow[3015,true];};
	
	[[_msg,name player,0],""clientMessage"",_to,false] spawn life_fnc_MP;
	[] call life_fnc_cellphone;
	hint format[""Vous avez envoyé un message à %1 : %2"",name _to,_msg];
	ctrlShow[3015,true];
";
//To All Cops
fnc_cell_textcop =
compileFinal "
	private[""_msg"",""_to""];
	_msg = _this select 0;
	_to = ""The Police"";
	if(_msg == """") exitWith {hint ""Vous devez saisir un message à envoyer !"";ctrlShow[3016,true];};
		
	[[_msg,name player,1],""clientMessage"",true,false] spawn life_fnc_MP;
	[[name player, position player],""life_fnc_createMarker"",west,false] spawn life_fnc_MP;
	hint format[""Vous avez envoyé un message à tous les Policiers : %2"",_to,_msg];
	ctrlShow[3016,true];
";
//To All Admins
fnc_cell_textadmin =
compileFinal "
	private[""_msg"",""_to"",""_from""];
	ctrlShow[3017,false];
	_msg = ctrlText 3003;
	_to = ""The Admins"";
	if(_msg == """") exitWith {hint ""Vous devez saisir un message à envoyer !"";ctrlShow[3017,true];};
		
	[[_msg,name player,2],""clientMessage"",true,false] spawn life_fnc_MP;
	[] call life_fnc_cellphone;
	hint format[""Vous avez envoyé un message à tous les Admin : %2"",_to,_msg];
	ctrlShow[3017,true];
";
//Admin To One Person
fnc_cell_adminmsg =
compileFinal "
	if(isServer) exitWith {};
	if((call life_adminlevel) < 1) exitWith {hint ""Vous n'êtes pas un Admin !"";};
	private[""_msg"",""_to""];
	_msg = ctrlText 3003;
	_to = call compile format[""%1"",(lbData[3004,(lbCurSel 3004)])];
	if(isNull _to) exitWith {};
	if(_msg == """") exitWith {hint ""Vous devez saisir un message à envoyer !"";};
	
	[[_msg,name player,3],""clientMessage"",_to,false] spawn life_fnc_MP;
	[] call life_fnc_cellphone;
	hint format[""Message Admin envoyé à : %1 - Message : %2"",name _to,_msg];
";

fnc_cell_adminmsgall =
compileFinal "
	if(isServer) exitWith {};
	if((call life_adminlevel) < 1) exitWith {hint ""Vous n'êtes pas un Admin !"";};
	private[""_msg"",""_from""];
	_msg = ctrlText 3003;
	if(_msg == """") exitWith {hint ""Vous devez saisir un message à envoyer !"";};
	
	[[_msg,name player,4],""clientMessage"",true,false] spawn life_fnc_MP;
	[] call life_fnc_cellphone;
	hint format[""Message Admin envoyé à tous les joueurs : %1"",_msg];
";
fnc_pub_msgall =
compileFinal "
	private[""_msg"",""_from""];
	_msg = ctrlText 3003;
	_from = ctrlText 3005;
	if(life_flouze < 50000) exitWith {hint ""Vous n'avez pas assez d'argent pour communiquer !""};
	if(_msg == """") exitWith {hint ""Vous devez entrer votre message de publicité"";};
	if(_from == """") exitWith {hint ""Vous devez signer votre message"";};
	
	[[_msg,_from,5],""TON_fnc_clientMessage"",true,false] spawn life_fnc_MP;
	life_flouze = life_flouze - 50000;

	hint format[""Altis TV Flash Publicité: %1"",_msg];
";
fnc_pub_loopmsgall =
compileFinal "
	private[""_msg"",""_from""];
	_msg = ctrlText 3003;
	_from = ctrlText 3005;
	if(life_flouze < 50000) exitWith {hint ""Vous n'avez pas assez d'argent pour communiquer !""};
	if(_msg == """") exitWith {hint ""Vous devez entrer votre message de publicité"";};
	if(_from == """") exitWith {hint ""Vous devez signer votre message"";};
	life_flouze = life_flouze - 200000;
	[[_msg,_from,5],""clientMessage"",true,false] spawn life_fnc_MP;
	hint format[""Altis TV Flash Publicité: %1"",_msg];
";
fnc_cell_depmsg =
compileFinal "
	private[""_msg"",""_to""];
	ctrlShow[3016,false];
	_msg = ctrlText 3003;
	_to = ""DEPANNEURS"";
	if(_msg == """") exitWith {hint ""Vous devez saisir un message à envoyer !"";ctrlShow[3016,true];};	
	[[_msg,name player,6],""clientMessage"",true,false] spawn life_fnc_MP;
	[] call life_fnc_cellphone;
	hint format[""Vous avez envoyé un message à tous les Dépanneurs : %2"",_to,_msg];
	[[name player, position player],""life_fnc_createMarkerDep"",civilian,false] call life_fnc_MP;
	ctrlShow[3016,true];
";
publicVariable "fnc_cell_textmsg";
publicVariable "fnc_cell_textcop";
publicVariable "fnc_cell_textadmin";
publicVariable "fnc_cell_adminmsg";
publicVariable "fnc_cell_adminmsgall";
publicVariable "fnc_cell_emsrequest";
publicVariable "fnc_pub_msgall";
publicVariable "fnc_pub_loopmsgall";
publicVariable "fnc_cell_depmsg";
publicVariable "fnc_cell_taximsg";
publicVariable "fnc_cell_medicmsg";
//Client Message
/*
	0 = private message
	1 = police message
	2 = message to admin
	3 = message from admin
	4 = admin message to all
	5 = Message pub
	6 = Message Dep
*/
	//diag_log format [""ClientMessage || _PosEmit: %1 _From :%2 _msg : %3"", _posEmit, _from, _msg];
TON_fnc_clientMessage =
compileFinal "
	if(isServer) exitWith {};
	private[""_msg"",""_from"", ""_type""];
	_msg = _this select 0;
	_from = _this select 1;
	_type = _this select 2;
	if(_from == """") exitWith {};
	switch (_type) do
	{
		case 0 :
		{
			private[""_message""];
			_message = format["">>>MESSAGE DE %1: %2"",_from,_msg];
			hint parseText format [""<t color='#FFCC00'><t size='2'><t align='center'>Nouveau Message<br/><br/><t color='#33CC33'><t align='left'><t size='1'>À: <t color='#ffffff'>Vous<br/><t color='#33CC33'>De: <t color='#ffffff'>%1<br/><br/><t color='#33CC33'>Message:<br/><t color='#ffffff'>%2"",_from,_msg];
			[""TextMessage"",[format[""Vous avez reçu un nouveau message privé de : %1"",_from]]] call bis_fnc_showNotification;
			systemChat _message;
		};
		
		case 1 :
		{
			if(side player != west) exitWith {};
			private[""_message"", ""_posEmit"", ""_Emitter""];
			_Emitter = _this select 3;
			_posEmit = position _Emitter;
			_message = format[""---Alerte de %1: %2"",_from,_msg];
			hint parseText format [""<t color='#316dff'><t size='2'><t align='center'>Nouvelle Alerte<br/><br/><t color='#33CC33'><t align='left'><t size='1'>À: <t color='#ffffff'>Tous les Policiers<br/><t color='#33CC33'>De: <t color='#ffffff'>%1<br/><br/><t color='#33CC33'>Message:<br/><t color='#ffffff'>%2"",_from,_msg];
			[name _Emitter, _posEmit] spawn life_fnc_createMarker;
			[""PoliceDispatch"",[format[""Nouvelle alerte de : %1"",_from]]] call bis_fnc_showNotification;
			systemChat _message;
		};
		
		case 2 :
		{
			if((call life_adminlevel) < 1) exitWith {};
			private[""_message""];
			_message = format[""???DEMANDE D'ADMIN de %1: %2"",_from,_msg];
			hint parseText format [""<t color='#ffcefe'><t size='2'><t align='center'>Demande d'Admin<br/><br/><t color='#33CC33'><t align='left'><t size='1'>À: <t color='#ffffff'>Admins<br/><t color='#33CC33'>De: <t color='#ffffff'>%1<br/><br/><t color='#33CC33'>Message:<br/><t color='#ffffff'>%2"",_from,_msg];
			
			[""AdminDispatch"",[format[""Nouvelle demande d'Admin de %1 !"",_from]]] call bis_fnc_showNotification;
			systemChat _message;
		};
		
		case 3 :
		{
			private[""_message""];
			_message = format[""!!!Message d'un ADMIN : %1"",_msg];
			_admin = format[""Envoyé par un Admin : %1"", _from];
			hint parseText format [""<t color='#FF0000'><t size='2'><t align='center'>Message d'un Admin<br/><br/><t color='#33CC33'><t align='left'><t size='1'>À: <t color='#ffffff'>Vous<br/><t color='#33CC33'>De: <t color='#ffffff'>Un Admin<br/><br/><t color='#33CC33'>Message:<br/><t color='#ffffff'>%1"",_msg];
			
			[""AdminMessage"",[""Vous avez reçu un message d'un Admin !""]] call bis_fnc_showNotification;
			systemChat _message;
			if((call life_adminlevel) > 0) then {systemChat _admin;};
		};
		
		case 4 :
		{
			private[""_message"",""_admin""];
			_message = format[""!!!Message d'un Admin à tous les joueurs : %1"",_msg];
			_admin = format[""Envoyé par un Admin : %1"", _from];
			hint parseText format [""<t color='#FF0000'><t size='2'><t align='center'>Message Admin<br/><br/><t color='#33CC33'><t align='left'><t size='1'>À: <t color='#ffffff'>Tous les joueurs<br/><t color='#33CC33'>D'un: <t color='#ffffff'>Admin<br/><br/><t color='#33CC33'>Message:<br/><t color='#ffffff'>%1"",_msg];
			
			[""AdminMessage"",[""Vous avez reçu un message d'un Admin !""]] call bis_fnc_showNotification;
			systemChat _message;
			if((call life_adminlevel) > 0) then {systemChat _admin;};
		};
		
		case 5 :
		{
			private[""_message""];
			_message = format[""ALTIS TV PUB: %1 Signature : %2"",_msg,_from];
			hint parseText format [""
			<t color='#ec0090'><t size='2.5'><t align='center'>Altis TV<br/><br/>
			<t color='#00c8ec'><t size='2'><t align='Left'>Flash Infos/Pub :<br/><br/>
			<t align='center'><t size='1.2'><t color='#33CC33'>%1<br/><br/>
			<t align='left'><t size='1'><t color='#ec008c'>%2<br/><br/>"",_msg,_from];
			[""PubMessage"",[""Nouvelle publicité""]] call bis_fnc_showNotification;
			systemChat _message;
		};	
		
		case 6 :
		{	
			if(!(license_civ_dep)) exitWith {};	
			if(side player == west) exitWith {};
			private[""_message"", ""_posEmit"" , ""_Emitter""];
			_Emitter = _this select 3;
			_posEmit = position _Emitter;
			_message = format[""---DEPANNEUR %1: %2"",_from,_msg];
			hint parseText format [""<t color='#ffff00'><t size='2'><t align='center'>Demande de dépannage<br/><br/><t color='#33CC33'><t align='left'><t size='1'>Pour: <t color='#ffffff'>Tous les dépanneurs<br/><t color='#33CC33'>De la part de: <t color='#ffffff'>%1<br/><br/><t color='#33CC33'>Message:<br/><t color='#ffffff'>%2"",_from,_msg];
			[""TextMessage"",[format[""Demande de dépannage: %1"",_from]]] call bis_fnc_showNotification;
			[name _Emitter, _posEmit] spawn life_fnc_createMarkerDep;
			systemChat _message;
			
		};
		
		case 7 :
		{
			private[""_message"",""_admin""];
			_message = format[""Message de la Police aux citoyens d'Altis : %1"",_msg];
			_admin = format[""Envoyé par : %1"", _from];
			hint parseText format [""<t color='#316dff'><t size='2'><t align='center'>Message de la Police<br/><br/><t color='#33CC33'><t align='left'><t size='1'>À: <t color='#ffffff'>Tous les citoyens<br/><t color='#33CC33'>De: <t color='#ffffff'>la Police<br/><br/><t color='#33CC33'>Message:<br/><t color='#ffffff'>%1"",_msg];
			
			[""PoliceDispatch"",[""Vous avez reçu un message de la Police !""]] call bis_fnc_showNotification;
			systemChat _message;
		
		};	
		case 8 :
		{
			private[""_message"", ""_posEmit"" , ""_Emitter""];
			_Emitter = _this select 3;
			_posEmit = position _Emitter;
			_message = format[""%2 de %1"",_from,_msg];
			hint parseText format [""<t color='#FFCC00'><t size='2'><t align='center'>Partage de position<br/><br/><t color='#33CC33'><t align='left'><t size='1'>À: <t color='#ffffff'>Vous<br/><t color='#33CC33'>De: <t color='#ffffff'>%1<br/>"",_from,_msg];
			[""GpsShareLucel"",[format[""Vous avez reçu la position GPS de %1"",_from]]] call bis_fnc_showNotification;
			[name _Emitter, _posEmit] spawn life_fnc_createMarkerCiv;
			systemChat _message;
		};
		
		case 9 :
		{	
			if(!(license_civ_taxi)) exitWith {};	
			if(side player == west) exitWith {};
			private[""_message"", ""_posEmit"" , ""_Emitter""];
			_Emitter = _this select 3;
			_posEmit = position _Emitter;
			_unit = player;
			_message = format[""---TAXI %1: %2"",_from,_msg];
			hint parseText format [""<t color='#ffff00'><t size='2'><t align='center'>Demande de Taxi<br/><br/><t color='#33CC33'><t align='left'><t size='1'>Pour: <t color='#ffffff'>Tous les conducteurs de taxi<br/><t color='#33CC33'>De la part de: <t color='#ffffff'>%1<br/><br/><t color='#33CC33'>Message:<br/><t color='#ffffff'>%2"",_from,_msg];
			[""TextMessage"",[format[""Demande de Taxi: %1"",_from]]] call bis_fnc_showNotification;
			[name _Emitter, _posEmit] spawn life_fnc_createMarkerTaxi;
			systemChat _message;
			
		};		
		case 10 :
		{	
			if(!(license_civ_medic)) exitWith {};	
			if(side player == west) exitWith {};
			private[""_message"", ""_posEmit"" , ""_Emitter""];
			_Emitter = _this select 3;
			_posEmit = position _Emitter;
			_unit = player;
			_message = format[""---MEDECIN %1: %2"",_from,_msg];
			hint parseText format [""<t color='#ffff00'><t size='2'><t align='center'>Demande de Secours<br/><br/><t color='#33CC33'><t align='left'><t size='1'>Pour: <t color='#ffffff'>Tous les médecins<br/><t color='#33CC33'>De la part de: <t color='#ffffff'>%1<br/><br/><t color='#33CC33'>Message:<br/><t color='#ffffff'>%2"",_from,_msg];
			[""TextMessage"",[format[""Demande de Secours : %1"",_from]]] call bis_fnc_showNotification;
			[name _Emitter, _posEmit] spawn life_fnc_createMarkerMedic;
			systemChat _message;
			
		};
		case 11 :
		{	
			if(!(license_civ_merc)) exitWith {};	
			if(side player == west) exitWith {};
			private[""_message"", ""_posEmit"" , ""_Emitter""];
			_Emitter = _this select 3;
			_posEmit = position _Emitter;
			_unit = player;
			_message = format[""---MERCENAIRE %1: %2"",_from,_msg];
			hint parseText format [""<t color='#ffff00'><t size='2'><t align='center'>Message aux mercenaires<br/><br/><t color='#33CC33'><t align='left'><t size='1'>Pour: <t color='#ffffff'>Tous les mercenaires<br/><t color='#33CC33'>De la part de: <t color='#ffffff'>%1<br/><br/><t color='#33CC33'>Message:<br/><t color='#ffffff'>%2"",_from,_msg];
			[""TextMessage"",[format[""Message aux mercenaires : %1"",_from]]] call bis_fnc_showNotification;
			systemChat _message;
			
		};
		
			
	};
";
publicVariable "TON_fnc_clientMessage";