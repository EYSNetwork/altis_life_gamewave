////////////////////////////////////////////////
// Player Actions
////////////////////////////////////////////////
FAR_Player_Actions =
{
	if (alive player && player isKindOf "Man") then 
	{
		// addAction args: title, filename, (arguments, priority, showWindow, hideOnUse, shortcut, condition, positionInModel, radius, radiusView, showIn3D, available, textDefault, textToolTip)
		player addAction ["<t color=""#AAF200"">" + "Massage cardiaque" + "</t>", "FAR_revive\FAR_handleAction.sqf", ["action_revive"], 10, true, true, "", "call FAR_Check_Revive"];
		player addAction ["<t color=""#AAF200"">" + "Morphine" + "</t>", "FAR_revive\FAR_handleAction.sqf", ["action_stabilize"], 10, true, true, "", "call FAR_Check_Stabilize"];
		player addAction ["<t color=""#FF0000"">" + "Achever la personne" + "</t>", "FAR_revive\FAR_handleAction.sqf", ["action_suicide"], 9, false, true, "", "call FAR_Check_Suicide"];
		player addAction ["<t color=""#FF9900"">" + "Deplacer corp" + "</t>", "FAR_revive\FAR_handleAction.sqf", ["action_drag"], 9, false, true, "", "call FAR_Check_Dragging"];
	};
};


////////////////////////////////////////////////
// Make Player Unconscious
////////////////////////////////////////////////

FAR_Player_Unconscious =
{
	private["_unit", "_killer"];
	_unit = _this select 0;
	_killer = _this select 1;
	
	// Death message
	if (FAR_EnableDeathMessages && !isNil "_killer" && isPlayer _killer && _killer != _unit) then
	{
		FAR_deathMessage = [_unit, _killer];
		publicVariable "FAR_deathMessage";
		["FAR_deathMessage", [_unit, _killer]] call FAR_public_EH;
	};
	// Eject unit if inside vehicle
	if (isPlayer _unit) then
	{
		disableUserInput true;
		titleText ["", "BLACK FADED"];
	};
	
	// Eject unit if inside vehicle
	if (vehicle _unit != _unit) then
	{
		unAssignVehicle _unit;
		_unit action ["eject", vehicle _unit];
		_unit action ["EngineOff", vehicle _unit];

		
		sleep 2;
	};
	_unit action ["eject", vehicle _unit];
	_unit setDamage 0;
	//life_is_arrested = true; //Verif pour les jojos qui leave en rea
	//life_alive = false;
	
	// SYNCRO REA
	life_in_rea = true;
	//[] call SOCK_fnc_updateRequest;
	[7] call SOCK_fnc_updatePartial;
    _unit setVelocity [0,0,0];
    _unit allowDamage false;
	_unit setCaptive true;
    _unit playMove "AinjPpneMstpSnonWrflDnon_rolltoback";
	
	sleep 4;
    
	if (isPlayer _unit) then
	{
		titleText ["", "BLACK IN", 1];
		disableUserInput false;

		// Mute ACRE
		_unit setVariable ["ace_sys_wounds_uncon", true];
	};
	
	_unit switchMove "AinjPpneMstpSnonWrflDnon";
	_unit enableSimulation false;
	_unit setVariable ["FAR_isUnconscious", 1, true];
	_unit allowDamage true;
	// Call this code only on players
	if (isPlayer _unit) then 
	{
		_bleedOut = time + FAR_BleedOut;
		
		while { !isNull _unit && alive _unit && _unit getVariable "FAR_isUnconscious" == 1 && _unit getVariable "FAR_isStabilized" == 0 && (FAR_BleedOut <= 0 || time < _bleedOut) } do
		{
			hintSilent format["Vous saignez, vous allez mourrir dans %1 secondes\n\n%2", round (_bleedOut - time), call FAR_CheckFriendlies];
			cutText["Vous êtes inconcient...","BLACK FADED"];
			sleep 0.5;
		};
		
		if (_unit getVariable "FAR_isStabilized" == 1) then {
			//Unit has been stabilized. Disregard bleedout timer and umute player
			_unit setVariable ["ace_sys_wounds_uncon", false];
			
			while { !isNull _unit && alive _unit && _unit getVariable "FAR_isUnconscious" == 1 } do
			{
				hintSilent format["Vous ne sentez plus la douleur", call FAR_CheckFriendlies];
				"chromAberration" ppEffectEnable true;
				"radialBlur" ppEffectEnable true;
				enableCamShake true;
				"chromAberration" ppEffectAdjust [random 0.25,random 0.25,true];
				"chromAberration" ppEffectCommit 1;   
				"radialBlur" ppEffectAdjust  [random 0.02,random 0.02,0.15,0.15];
				"radialBlur" ppEffectCommit 1;
				addcamShake[random 3, 1, random 3];
				sleep 0.5;
			};
		};
		
		// Player bled out
		if (FAR_BleedOut > 0 && {time > _bleedOut} && {_unit getVariable ["FAR_isStabilized",0] == 0}) then
		{
			//life_is_arrested = false;
			//life_alive = true;
			_unit setDamage 1;
			hint "Vous êtes mort.";
		}
		else
		{
			// Player got revived
			_unit setVariable ["FAR_isStabilized", 0, true];
			sleep 6;
			
			// Clear the "medic nearby" hint
			hintSilent "";

			// Unmute ACRE
			if (isPlayer _unit) then
			{
				_unit setVariable ["ace_sys_wounds_uncon", false];
			};
			_unit enableSimulation true;
			_unit allowDamage true;
			_unit setDamage 0;
			_unit setCaptive false;
			_unit setDamage 0.95;
			life_alive = true;
			life_in_rea = false;
			
			// SYNCRO REA
			//[] call SOCK_fnc_updateRequest;
			[7] call SOCK_fnc_updatePartial;

			_unit playMove "amovppnemstpsraswrfldnon";
			_unit playMove "";
			
			if (alive _unit) then
			{
			"chromAberration" ppEffectEnable true;
			"radialBlur" ppEffectEnable true;
			enableCamShake true;
			for "_i" from 0 to 120 do
			{
				"chromAberration" ppEffectAdjust [random 0.25,random 0.25,true];
				"chromAberration" ppEffectCommit 1;   
				"radialBlur" ppEffectAdjust  [random 0.02,random 0.02,0.15,0.15];
				"radialBlur" ppEffectCommit 1;
				addcamShake[random 3, 1, random 3];
				sleep 0.7;
			};
			};
			//Stop effects
			"chromAberration" ppEffectAdjust [0,0,true];
			"chromAberration" ppEffectCommit 5;
			"radialBlur" ppEffectAdjust  [0,0,0,0];
			"radialBlur" ppEffectCommit 5;
			sleep 6;

			//Deactivate ppEffects
			"chromAberration" ppEffectEnable false;
			"radialBlur" ppEffectEnable false;
			resetCamShake;
		
		};
	}
	else
	{
		// [Debugging] Bleedout for AI
		_bleedOut = time + FAR_BleedOut;
		
		while { !isNull _unit && alive _unit && _unit getVariable "FAR_isUnconscious" == 1 && _unit getVariable "FAR_isStabilized" == 0 && (FAR_BleedOut <= 0 || time < _bleedOut) } do
		{
			sleep 0.5;
		};
		
		if (_unit getVariable "FAR_isStabilized" == 1) then {			
			while { !isNull _unit && alive _unit && _unit getVariable "FAR_isUnconscious" == 1 } do
			{
				sleep 0.5;
			};
		};
		
		// AI bled out
		if (FAR_BleedOut > 0 && {time > _bleedOut} && {_unit getVariable ["FAR_isStabilized",0] == 0}) then
		{
			_unit setDamage 1;
			_unit setVariable ["FAR_isUnconscious", 0, true];
			_unit setVariable ["FAR_isDragged", 0, true];
		}
	};
};

////////////////////////////////////////////////
// Revive Player
////////////////////////////////////////////////
FAR_HandleRevive =
{
	private ["_target"];

	_target = _this select 0;

	if (alive _target && !license_civ_medic && playerSide == civilian && animationState player != "AinvPknlMstpSnonWnonDr_medic0") then
	{
			hint "Vous n'êtes pas médecin !";
	};
	
	if (alive _target && license_civ_medic && playerSide == civilian && animationState player != "AinvPknlMstpSnonWnonDr_medic0") then
	{
	
		if( random(100) < 45) then
			{
				player playMoveNow "AinvPknlMstpSnonWnonDr_medic0";
				sleep 6;
				_target setVariable ["FAR_isUnconscious", 0, true];
				_target setVariable ["FAR_isDragged", 0, true];
				hint "Vous avez sauvé une vie !";
				player playActionNow "stop";
			}
		else
			{
			player playMoveNow "AinvPknlMstpSnonWnonDr_medic0";
			sleep 6;
			hint "Le patient ne réagit pas...";
			player playActionNow "stop";
			};
	};

	if (alive _target && playerSide == west && (player getVariable "imMedicBro") && animationState player != "AinvPknlMstpSnonWnonDr_medic0") then
	{
		if( random(100) < 45) then
			{
				player playMoveNow "AinvPknlMstpSnonWnonDr_medic0";
				sleep 6;
				_target setVariable ["FAR_isUnconscious", 0, true];
				_target setVariable ["FAR_isDragged", 0, true];
				hint "Vous avez sauvé une vie !";
				player playActionNow "stop";
			}
		else
			{
			player playMoveNow "AinvPknlMstpSnonWnonDr_medic0";
			sleep 6;
			hint "Le patient ne réagit pas...";
			player playActionNow "stop";
			};
	};

	if (alive _target && playerSide == west && !(player getVariable "imMedicBro") && animationState player != "AinvPknlMstpSnonWnonDr_medic0") then
	{
		if( random(100) < 5) then
			{
				player playMoveNow "AinvPknlMstpSnonWnonDr_medic0";
				sleep 6;
				_target setVariable ["FAR_isUnconscious", 0, true];
				_target setVariable ["FAR_isDragged", 0, true];
				hint "Vous avez sauvé une vie !";
				player playActionNow "stop";
			}
		else
			{
			player playMoveNow "AinvPknlMstpSnonWnonDr_medic0";
			sleep 6;
			hint "Le patient ne réagit pas...";
			player playActionNow "stop";
			};
	};
};

////////////////////////////////////////////////
// Achever
////////////////////////////////////////////////
FAR_Kill =
{
private ["_target"];
_target = _this select 0;
[[getPlayerUID player,name player,"187"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
player playMove "AinvPknlMstpSnonWnonDr_medic0";
sleep 3;
_target setDamage 1;
player playActionNow "stop";

};

////////////////////////////////////////////////
// Stabilize Player
////////////////////////////////////////////////
FAR_HandleStabilize =
{
	private ["_target"];

	_target = _this select 0;

	if (alive _target) then
	{
		player playMove "AinvPknlMstpSnonWnonDr_medic0";
		
		if (!("Medikit" in (items player)) ) then {
			player removeItem "FirstAidKit";
		};

		_target setVariable ["FAR_isStabilized", 1, true];
		
		sleep 6;
	};
};

////////////////////////////////////////////////
// Drag Injured Player
////////////////////////////////////////////////
FAR_Drag =
{
	private ["_target", "_id"];
	
	FAR_isDragging = true;

	_target = _this select 0;

	_target attachTo [player, [0, 1.1, 0.092]];
	_target setDir 180;
	_target setVariable ["FAR_isDragged", 1, true];
	
	player playMoveNow "AcinPknlMstpSrasWrflDnon";
	
	// Rotation fix
	FAR_isDragging_EH = _target;
	publicVariable "FAR_isDragging_EH";
	
	// Add release action and save its id so it can be removed
	_id = player addAction ["<t color=""#FF9900"">" + "Lacher corp" + "</t>", "FAR_revive\FAR_handleAction.sqf", ["action_release"], 10, true, true, "", "true"];
	
	hint "Appuyez sur la touche 'C' si vous etes bloque";
	
	// Wait until release action is used
	waitUntil 
	{ 
		!alive player || player getVariable "FAR_isUnconscious" == 1 || !alive _target || _target getVariable "FAR_isUnconscious" == 0 || !FAR_isDragging || _target getVariable "FAR_isDragged" == 0 
	};

	// Handle release action
	FAR_isDragging = false;
	
	if (!isNull _target && alive _target) then
	{
		_target switchMove "AinjPpneMstpSnonWrflDnon";
		_target setVariable ["FAR_isDragged", 0, true];
		detach _target;
	};
	
	player removeAction _id;
};

FAR_Release =
{
	// Switch back to default animation
	player playMove "amovpknlmstpsraswrfldnon";

	FAR_isDragging = false;
};

////////////////////////////////////////////////
// Event handler for public variables
////////////////////////////////////////////////
FAR_public_EH =
{
	if(count _this < 2) exitWith {};
	
	_EH  = _this select 0;
	_target = _this select 1;

	// FAR_isDragging
	if (_EH == "FAR_isDragging_EH") then
	{
		_target setDir 180;
	};
	
	// FAR_deathMessage
	if (_EH == "FAR_deathMessage") then
	{
		_killed = _target select 0;
		_killer = _target select 1;

		if (isPlayer _killed && isPlayer _killer) then
		{
			systemChat format["%2 a mis inconscient %1", name _killed, name _killer];
			//[[0,format["%2 a mis inconscient %1", name _killed, name _killer]],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
		};				
	};
};

////////////////////////////////////////////////
// Revive Action Check
////////////////////////////////////////////////
FAR_Check_Revive = 
{
	private ["_target", "_isTargetUnconscious", "_isDragged"];

	_return = false;
	
	// Unit that will excute the action
	_isPlayerUnconscious = player getVariable "FAR_isUnconscious";
	_isMedic = getNumber (configfile >> "CfgVehicles" >> typeOf player >> "attendant");
	_target = cursorTarget;

	// Make sure player is alive and target is an injured unit
	if( !alive player || _isPlayerUnconscious == 1 || FAR_isDragging || isNil "_target" || !alive _target || (!isPlayer _target && !FAR_Debugging) || (_target distance player) > 2 ) exitWith
	{
		_return
	};

	_isTargetUnconscious = _target getVariable "FAR_isUnconscious";
	_isDragged = _target getVariable "FAR_isDragged"; 
	
	// Make sure target is unconscious and player is a medic 
	if (_isTargetUnconscious == 1 && _isDragged == 0 && (_isMedic == 1 || FAR_ReviveMode > 0) ) then
	{
		_return = true;

		// [ReviveMode] Check if player has a Medikit
		if ( FAR_ReviveMode == 2 && !("Medikit" in (items player)) ) then
		{
			_return = false;
		};
	};
	
	_return
};

////////////////////////////////////////////////
// Stabilize Action Check
////////////////////////////////////////////////
FAR_Check_Stabilize = 
{
	private ["_target", "_isTargetUnconscious", "_isDragged"];

	_return = false;
	
	// Unit that will excute the action
	_isPlayerUnconscious = player getVariable "FAR_isUnconscious";
	_target = cursorTarget;
	

	// Make sure player is alive and target is an injured unit
	if( !alive player || _isPlayerUnconscious == 1 || FAR_isDragging || isNil "_target" || !alive _target || (!isPlayer _target && !FAR_Debugging) || (_target distance player) > 2 ) exitWith
	{
		_return
	};

	_isTargetUnconscious = _target getVariable "FAR_isUnconscious";
	_isTargetStabilized = _target getVariable "FAR_isStabilized";
	_isDragged = _target getVariable "FAR_isDragged"; 
	
	// Make sure target is unconscious and hasn't been stabilized yet, and player has a FAK/Medikit 
	if (_isTargetUnconscious == 1 && _isTargetStabilized == 0 && _isDragged == 0 && ( ("FirstAidKit" in (items player)) || ("Medikit" in (items player)) ) ) then
	{
		_return = true;
	};
	
	_return
};

////////////////////////////////////////////////
// Suicide Action Check
////////////////////////////////////////////////
FAR_Check_Suicide =
{
	_return = false;
	_target = cursorTarget;
	_isPlayerUnconscious = _target getVariable ["FAR_isUnconscious",0];
	
	if (alive _target 
	&& player distance cursorTarget < 3.5 
	&& _isPlayerUnconscious == 1 
	&& (currentWeapon player == primaryWeapon player OR currentWeapon player == handgunWeapon player) 
	&& currentWeapon player != ""	
	) then 
	{
		_return = true;
	};
	
	_return
};

////////////////////////////////////////////////
// Dragging Action Check
////////////////////////////////////////////////
FAR_Check_Dragging =
{
	private ["_target", "_isPlayerUnconscious", "_isDragged"];
	
	_return = false;
	_target = cursorTarget;
	_isPlayerUnconscious = player getVariable "FAR_isUnconscious";

	if( !alive player || _isPlayerUnconscious == 1 || FAR_isDragging || isNil "_target" || !alive _target || (!isPlayer _target && !FAR_Debugging) || (_target distance player) > 2 ) exitWith
	{
		_return;
	};
	
	// Target of the action
	_isTargetUnconscious = _target getVariable "FAR_isUnconscious";
	_isDragged = _target getVariable "FAR_isDragged"; 
	
	if(_isTargetUnconscious == 1 && _isDragged == 0) then
	{
		_return = true;
	};
		
	_return
};

////////////////////////////////////////////////
// Show Nearby Friendly Medics
////////////////////////////////////////////////
FAR_IsFriendlyMedic =
{
	private ["_unit"];
	
	_return = false;
	_unit = _this;
	_isMedic = getNumber (configfile >> "CfgVehicles" >> typeOf _unit >> "attendant");
				
	if ( alive _unit && (isPlayer _unit || FAR_Debugging) && side _unit == FAR_PlayerSide && _unit getVariable "FAR_isUnconscious" == 0 && (_isMedic == 1 || FAR_ReviveMode > 0) ) then
	{
		_return = true;
	};
	
	_return
};

FAR_CheckFriendlies =
{
	private ["_unit", "_units", "_medics", "_hintMsg"];
	
	_units = nearestObjects [getpos player, ["Man", "Car", "Air", "Ship"], 800];
	_medics = [];
	_dist = 800;
	_hintMsg = "";
	
	// Find nearby friendly medics
	if (count _units > 1) then
	{
		{
			if (_x isKindOf "Car" || _x isKindOf "Air" || _x isKindOf "Ship") then
			{
				if (alive _x && count (crew _x) > 0) then
				{
					{
						if (_x call FAR_IsFriendlyMedic) then
						{
							_medics = _medics + [_x];
							
							if (true) exitWith {};
						};
					} forEach crew _x;
				};
			} 
			else 
			{
				if (_x call FAR_IsFriendlyMedic) then
				{
					_medics = _medics + [_x];
				};
			};
			
		} forEach _units;
	};
	
	// Sort medics by distance
	if (count _medics > 0) then
	{
		{
			if (player distance _x < _dist) then
			{
				_unit = _x;
				_dist = player distance _x;
			};
		
		} forEach _medics;
		
		if (!isNull _unit) then
		{
			_unitName	= name _unit;
			_distance	= floor (player distance _unit);
			
			_hintMsg = format["Quelqu'un est proche de vous.", _unitName, _distance];
		};
	} 
	else 
	{
		_hintMsg = "Personne à proximité";
	};
	
	_hintMsg
};



