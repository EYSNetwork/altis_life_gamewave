/*
	File: fn_welcomeNotification.sqf
	
	Description:
	Called upon first spawn selection and welcomes our player.

format["Bienvenue %1",name player] hintC
[
	"Site Web : http://www.altislife.fr",
	"TEAMSPEAK : ts.gamewave.fr",
	"Vous jouez sur la mission Altis Life RPG 3.1.3 par TONIC Modifié par Altislife.fr",
	"Menu d'intéraction (Windows Gauche) : Réparer Vehicule / Réanimation /Ramasser Argent,Tortue... / Menu Police",
	"MAJ + G : Se rendre",
	"MAJ + F : Assommer",
	"MAJ + H / CTRL + H : Ranger/Sortir une Arme",
	"MAJ + ESPACE : Saut",
	"MAJ + R : Menotter (Police)",
	"MAJ + L : Gyrophare (Police)"
	
];

*/
"" hintC parseText "<t><t size='1.5'>Bienvenue sur Altislife France</t><br/>
<br/>
<t>Site Web : </t><t color='#AAF200'>http://www.altislife.fr</t><br/>
<t>TeamSpeak : </t><t color='#AAF200'>ts.gamewave.fr</t><br/>
</t><t color='#FF0000'>Consultez les règles avant de jouer !</t><br/>
<br/>
<t color='#AAF200'>Menu d'interaction (Windows Gauche) : </t>Menu Métier (Taxi/Médecin/Depaneur) /<t> Menu Maison / Menu Véhicule / Démenottage / Escorte / Réanimation / Ramasser Objets(Tortue/Argent) /</t><t color='#21c3ff'>Menu Police</t><br/>
<t color='#AAF200'>U : </t><t>Ouvrir Véhicule/Maison</t><br/>
<t color='#AAF200'>MAJ + G : </t><t>Se rendre</t><br/>
<t color='#AAF200'>MAJ + F : </t><t>Assommer</t><br/>
<t color='#AAF200'>MAJ + H / CTRL + H : </t><t>Ranger/Dégainer Arme</t><br/>
<t color='#AAF200'>MAJ + ESPACE : </t><t>Saut</t><br/>
<t color='#21c3ff'>MAJ + R : </t><t>Menotter (Police)</t><br/>
<t color='#21c3ff'>MAJ + L : </t><t>Gyrophare (Police et Médecin)</t><br/>
<br/>
<t color='#FF0000'>Vous jouez sur la mission Altis Life RPG 3.1.4.8 créée par TONIC et modifié par l'équipe Altislife.fr.</t><br/>
<t color='#FF0000'>Des bugs peuvent subvenir, n'oubliez pas que c'est un mod en BETA et en constante évolution.</t><br/>"
;
