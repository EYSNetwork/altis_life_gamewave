#include <macro.h>
/*
	File: fn_virt_sell.sqf
	Author: Bryan "Tonic" Boardwine
	Edited by worldtrade1101
	
	Description:
	Sell a virtual item to the store / shop
*/
if((time - life_action_delay) < 5) exitWith {hint "Attendez 5 secondes avant la prochaine vente."};
life_action_delay = time;
private["_type","_index","_price","_var","_amount","_name","_joueurs","_coplist","_copfactor"];
if((lbCurSel 2402) == -1) exitWith {};

_type = lbData[2402,(lbCurSel 2402)];
_price = lbValue[2402,(lbCurSel 2402)];
//_var = [_type,0] call life_fnc_varHandle;


_amount = ctrlText 2405;
if(!([_amount] call TON_fnc_isnumber)) exitWith {hint "Tu n'as pas choisis de quantité !";};
_amount = parseNumber (_amount);
if(_amount > (missionNameSpace getVariable _var)) exitWith {hint "Tu n'as pas tant à vendre !"};

if(life_shop_type == "heroin") then
{

	_price = (_price * _amount);
	_name = [([_type,0] call life_fnc_varHandle)] call life_fnc_varToStr;
	if(([false,_type,_amount] call life_fnc_handleInv)) then
	{
		hint format["Tu as vendu %1 %2 pour %3€",_amount,_name,[_price] call life_fnc_numberText];
		life_flouze = life_flouze + _price;
		[] call life_fnc_virt_updateEco;
		[[0,player,life_shop_type,_amount,_price,_type],"TON_fnc_Ajustprices",false,false] spawn life_fnc_MP;
	};

	private["_array","_ind","_val"];
	_array = life_shop_npc getVariable["sellers",[]];
	_ind = [getPlayerUID player,_array] call TON_fnc_index;
	if(_ind != -1) then
	{
		_val = (_array select _ind) select 2;
		_val = _val + _price;
		_array set[_ind,[getPlayerUID player,profileName,_val]];
		life_shop_npc setVariable["sellers",_array,true];
	}
		else
	{
		_array set[count _array,[getPlayerUID player,profileName,_price]];
		life_shop_npc setVariable["sellers",_array,true];
	};
}else{
	_price = (_price * _amount);
	_name = [([_type,0] call life_fnc_varHandle)] call life_fnc_varToStr;
	if(([false,_type,_amount] call life_fnc_handleInv)) then
	{
		hint format["Tu as vendu %1 %2 pour %3€",_amount,_name,[_price] call life_fnc_numberText];
		life_flouze = life_flouze + _price;
		[] call life_fnc_virt_updateEco;
		[[0,player,life_shop_type,_amount,_price,_type],"TON_fnc_Ajustprices",false,false] spawn life_fnc_MP;
	};
};
//[0] call SOCK_fnc_updatePartial;
