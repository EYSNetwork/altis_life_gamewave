#include <macro.h>
/*
	File: fn_weaponShopBuySell.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master handling of the weapon shop for buying / selling an item.
*/
disableSerialization;
private["_price","_item","_itemInfo","_bad"];
if((lbCurSel 38403) == -1) exitWith {hint "You need to select an item to buy/sell."};
_price = lbValue[38403,(lbCurSel 38403)]; if(isNil "_price") then {_price = 0;};
_item = lbData[38403,(lbCurSel 38403)];
_itemInfo = [_item] call life_fnc_fetchCfgDetails;

_bad = "";

if((_itemInfo select 6) != "CfgVehicles") then
{
	if((_itemInfo select 4) in [4096,131072]) then
	{
		if(!(player canAdd _item) && (uiNamespace getVariable["Weapon_Shop_Filter",0]) != 1) exitWith {_bad = "Vous n'avez pas assez de place pour l'acheter !"};
	};
};

if(_bad != "") exitWith {hint _bad};

if((uiNamespace getVariable["Weapon_Shop_Filter",0]) == 1) then
{
	life_flouze = life_flouze + _price;
	[_item,false] call life_fnc_handleItem;
	hint parseText format["Vous avez vendu un(e) %1 pour <t color='#8cff9b'>%2€</t>",_itemInfo select 1,[_price] call life_fnc_numberText];
	[nil,(uiNamespace getVariable["Weapon_Shop_Filter",0])] call life_fnc_weaponShopFilter; //Update the menu.
}
	else
{
	private["_hideout"];
	_hideout = (nearestObjects[getPosATL player,["Land_u_Barracks_V2_F","Land_i_Barracks_V2_F"],25]) select 0;
	if(!isNil "_hideout" && {!isNil {grpPlayer getVariable "gang_bank"}} && {(grpPlayer getVariable "gang_bank") >= _price}) then {
		_action = [
			format["Le gang n'a suffisamment pas assez de fonds pour vous payer ceci, voulez vous payer ceci de votre poche ?<br/><br/>Fonds du Gang : <t color='#8cff9b'>%1€</t><br/>Votre Argent : <t color='#8cff9b'>%2€</t>",
				[(grpPlayer getVariable "gang_bank")] call life_fnc_numberText,
				[life_flouze] call life_fnc_numberText
			],
			"Pay with cash or gang funds",
			"Gang Funds",
			"My Cash"
		] call BIS_fnc_guiMessage;
		if(_action) then {
			hint parseText format["Vous avez acheté un(e) %1 pour <t color='#8cff9b'>%2€</t> avec les fond financiers du gang.",_itemInfo select 1,[_price] call life_fnc_numberText];
			_funds = grpPlayer getVariable "gang_bank";
			_funds = _funds - _price;
			grpPlayer setVariable["gang_bank",_funds,true];
			[_item,true] spawn life_fnc_handleItem;
			[[1,grpPlayer],"TON_fnc_updateGang",false,false] spawn life_fnc_MP;
		} else {
			if(_price > life_flouze) exitWith {hint "Vous n'avez pas assez d'argent !"};
			hint parseText format["Vous avez acheté un(e) %1 pour <t color='#8cff9b'>%2€</t>",_itemInfo select 1,[_price] call life_fnc_numberText];
			__SUB__(life_flouze,_price);
			[_item,true] spawn life_fnc_handleItem;
			playSound "caching";
		};
	} else {
		if(_price > life_flouze) exitWith {hint "Vous n'avez pas assez d'argent !"};
		hint parseText format["Vous avez acheté un(e) %1 pour <t color='#8cff9b'>%2€</t>",_itemInfo select 1,[_price] call life_fnc_numberText];
		life_flouze = life_flouze - _price;
		playSound "caching";
		[_item,true] spawn life_fnc_handleItem;
		//[[getPlayerUID player,playerSide,life_flouze,0],"DB_fnc_updatePartial",false,false] spawn life_fnc_MP; // Sync du life_flouze 
	};
};
//Hotfix in for cop gear
//if(playerSide == west) then
//{
//	[] call life_fnc_saveGear;
//};
[] call life_fnc_saveGear;
