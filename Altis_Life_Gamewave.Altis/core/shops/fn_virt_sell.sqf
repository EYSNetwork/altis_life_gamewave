#include <macro.h>
/*
	File: fn_virt_sell.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Sell a virtual item to the store / shop
*/
if((time - life_action_delay) < 2.5) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_type","_index","_price","_var","_amount","_name"];
if((lbCurSel 2402) == -1) exitWith {};
_type = lbData[2402,(lbCurSel 2402)];
_index = [_type,__GETC__(sell_array)] call TON_fnc_index;
if(_index == -1) exitWith {};
_price = (__GETC__(sell_array) select _index) select 1;
//_price = [select 1] life_fnc_queryPrices 1;
_var = [_type,0] call life_fnc_varHandle;

_amount = ctrlText 2405;
if(!([_amount] call TON_fnc_isnumber)) exitWith {hint "Tu n'as pas choisis de quantité !";};
_amount = parseNumber (_amount);
if(_amount > (missionNameSpace getVariable _var)) exitWith {hint "Tu n'as pas tant à vendre !"};

_price = (_price * _amount);

if(life_shop_type == "blackmarket") then
{
		_price = (_price * 1.2);
};

_name = [_var] call life_fnc_vartostr;

if(([false,_type,_amount] call life_fnc_handleInv)) then
{
	playSound "caching";
	hint format["Tu as vendu %1 %2 pour %3€",_amount,_name,[_price] call life_fnc_numberText];
	life_flouze = life_flouze + _price;
	diag_log format ["VirtSell Names %1 | _var %2 | _amount %3 | _price %4", _name, _var, _amount, _price];
	//[_name,_amount,_price,_var] call SOCK_fnc_pricesUpdate;	
	[] call life_fnc_virt_update;
	
};



// QUESTION DEALER ????
if(life_shop_type == "heroin") then
{
	private["_array","_ind","_val"];
	_array = life_shop_npc getVariable["sellers",[]];
	_ind = [getPlayerUID player,_array] call TON_fnc_index;
	if(_ind != -1) then
	{
		_val = (_array select _ind) select 2;
		_val = _val + _price;
        _array set[_ind,[getPlayerUID player,profileName,_val]];
		life_shop_npc setVariable["sellers",_array,true];
	}
		else
	{
        _array pushBack [getPlayerUID player,profileName,_price];
		life_shop_npc setVariable["sellers",_array,true];
	};
};
//[0] call SOCK_fnc_updatePartial;
