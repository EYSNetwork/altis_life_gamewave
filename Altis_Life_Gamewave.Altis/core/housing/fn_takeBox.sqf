/*
Altislife.fr MOFO
started by olek
finished by lucel
*/


private["_house","_containers","_player","_curtarget","_action"];
_curtarget = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
_player = player;
_house = nearestBuilding (getPosATL player);
_containers = _house getVariable["containers",[]];
   // diag_log format ["_house %1",_house];
   //diag_log format ["_containers %1",_containers];
   // diag_log format ["_player %1",_player];

	
if(((_house getVariable "house_owner") select 0) != (getPlayerUID player)) exitwith {hint "Ce coffre ne vous appartient pas."};
if(!(_house in life_vehicles)) exitWith {hint "Ce coffre ne vous appartient pas."};		

_action = [
	"Êtes-vous sûr de vouloir récupérer votre coffre de maison ? Tout le matériel a l'intérieur sera définitivement perdu.",
	"Prendre Coffre",
	"Oui",
	"Non"
] call BIS_fnc_guiMessage;

if(_action) then {
    if (_curtarget isKindOf "Box_IND_Grenades_F") then {
            deleteVehicle _curtarget;
			_containers = _containers - [_curtarget];
			//diag_log format ["_containers AFTER DELETE %1",_containers];
			_house setVariable["containers",_containers,true];
			[[_house],"TON_fnc_removeHouseContainers",false,false] spawn life_fnc_MP;
			[true,"storagesmall",1] call life_fnc_handleInv;
			titleText[format["Vous avez récupéré un petit coffre"],"PLAIN"];
		};
				
				
    if (_curtarget isKindOf "B_supplyCrate_F") then {
	        deleteVehicle _curtarget;
			_containers = _containers - [_curtarget];
			//diag_log format ["_containers AFTER DELETE %1",_containers];
			_house setVariable["containers",_containers,true];
			[[_house],"TON_fnc_removeHouseContainers",false,false] spawn life_fnc_MP;
			[true,"storagebig",1] call life_fnc_handleInv;	
			titleText[format["Vous avez récupéré un grand coffre"],"PLAIN"];
		};
};