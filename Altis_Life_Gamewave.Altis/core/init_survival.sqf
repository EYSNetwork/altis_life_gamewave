[] spawn  {
	private["_fnc_food","_fnc_water"];
	_fnc_food = 
	{
		if(life_hunger < 2) then {player setDamage 1; hint "You have starved to death.";}
		else
		{
		life_hunger = life_hunger - 10;
		[] call life_fnc_hudUpdate;
		if(life_hunger < 2) then {player setDamage 1; hint "You have starved to death.";};
		switch(life_hunger) do {
			case 30: {hint "Vous n'avez rien mangé depuis un moment, vous devez vous nourrir rapidement !";};
			case 20: {hint "Vous commencez à mourir de faim, vous devez vous nourrir, sinon vous allez mourir.";};
			case 10: {hint "Vous êtes maintenant affamé, vous allez mourir dans très peu de temps si vous ne mangez pas quelque chose.";player setFatigue 1;};
			};
		};
	};
	
	_fnc_water = 
	{
		if(life_thirst < 2) then {player setDamage 1; hint "Vous êtes mort de déshydratation.";}
		else
		{
			life_thirst = life_thirst - 10;
			[] call life_fnc_hudUpdate;
			if(life_thirst < 2) then {player setDamage 1; hint "Vous êtes mort de déshydratation.";};
			switch(life_thirst) do 
			{
				case 30: {hint"Vous commencez à être assoiffé, vous devez boire rapidement !";};
				case 20: {hint "Vous n'avez rien bu depuis longtemps, vous n'avez rien bu depuis longtemps, vous devriez trouver quelque chose à boire avant de mourir de déshydratation"; player setFatigue 1;};
				case 10: {hint "Vous souffrez actuellement de déshydratation sévère, vous êtes dans l'obligation de trouver quelque chose à boire rapidement!"; player setFatigue 1;};
			};
		};
	};
	
	while{true} do
	{
		sleep 600;
		[] call _fnc_water;
		sleep 250;
		[] call _fnc_food;
	};
};

[] spawn
{
	private["_bp","_load","_cfg"];
	while{true} do
	{
		waitUntil {backpack player != ""};
		_bp = backpack player;
		_cfg = getNumber(configFile >> "CfgVehicles" >> (backpack player) >> "maximumload");
		_load = round(_cfg / 8);
		life_maxWeight = life_maxWeightT + _load;
		waitUntil {backpack player != _bp};
		if(backpack player == "") then 
		{
			life_maxWeight = life_maxWeightT;
		};
	};
};

//[] spawn
//{
//    while {true} do
//    {
//        sleep 1.5;
//        if(life_carryWeight > life_maxWeight && !isForcedWalk player) then {
//            player forceWalk true;
//            player setFatigue 1;
//            hint "Vous portez trop de poids, vous n'êtes pas en mesure de courrir ou de vous déplacer rapidement à moins de déposer certains articles!";
//        } else {
//            if(isForcedWalk player) then {
//                player forceWalk false;
//            };
//        };
//    };
//};


[] spawn  
{
	private["_walkDis","_myLastPos","_MaxWalk","_runHunger","_runDehydrate"];
	_walkDis = 0;
	_myLastPos = (getPos player select 0) + (getPos player select 1);
	_MaxWalk = 1200;
	while{true} do 
	{
		sleep 0.5;
		if(!alive player) then {_walkDis = 0;}
		else
		{
			_CurPos = (getPos player select 0) + (getPos player select 1);
			if((_CurPos != _myLastPos) && (vehicle player == player)) then
			{
				_walkDis = _walkDis + 1;
				if(_walkDis == _MaxWalk) then
				{
					_walkDis = 0;
					life_thirst = life_thirst - 5;
					life_hunger = life_hunger - 5;
					[] call life_fnc_hudUpdate;
				};
			};
			_myLastPos = (getPos player select 0) + (getPos player select 1);
		};
	};
};