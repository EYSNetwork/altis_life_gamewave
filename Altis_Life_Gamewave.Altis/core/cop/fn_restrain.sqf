/*
	File: fn_restrain.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Retrains the client.
*/
private["_cop","_player","_members","_unitID"];
_cop = [_this,0,Objnull,[Objnull]] call BIS_fnc_param;
_player = player;
_members = group player getVariable "gang_members";
_unitID = getPlayerUID player;
if(isNull _cop) exitWith {};

//Monitor excessive restrainment
[] spawn
{
	private["_time"];
	while {true} do
	{
		_time = time;
		waitUntil {(time - _time) > (15 * 60)};
		
		if(!(player getVariable["restrained",FALSE])) exitWith {};
		if((!([west,getPos player,60] call life_fnc_nearUnits) || !([civilian,getPos player,60] call life_fnc_nearUnits)) && (player getVariable["restrained",FALSE]) && vehicle player == player) exitWith {
			life_restrained = false;
			player setVariable["restrained",FALSE,TRUE];
			player setVariable["CopRestrain",false,true];
			player setVariable["surrender",false,true];
			player setVariable["Escorting",FALSE,TRUE];
			player setVariable["transporting",false,true];
			detach player;
			[] spawn life_fnc_initGang;
			titleText["Vous êtes liberé automatiquement après 15 minutes","PLAIN"];
		};
	};
};

if((player getVariable["surrender",FALSE])) then { player setVariable["surrender",FALSE,TRUE]; player switchMove ""; };
titleText[format["Vous êtes menotté par quelqu'un",name _cop],"PLAIN"];
[[player, "Cuff",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;

//SYNCRO MENOTAGE
life_restrained = true;
[7] call SOCK_fnc_updatePartial;

if(playerSide == civilian)then{
//_members = _members - [_unitID];
//group player setVariable["gang_members",_members,true];
//[[player,group player],"clientGangKick",player,false] spawn life_fnc_MP;
closeDialog 0;
[player] joinSilent (createGroup civilian);
};

				
while {player getVariable "restrained"} do
	{
	
	if(vehicle player == player) then {
		player playMove "AmovPercMstpSnonWnonDnon_Ease";
	};
	_state = vehicle player;
	waitUntil {animationState player != "AmovPercMstpSnonWnonDnon_Ease" || !(player getvariable "restrained") || vehicle player != _state};
			
	if(!alive player) exitWith
	{
		life_restrained = false;
		player setVariable["restrained",false,true];
		player setVariable["CopRestrain",false,true];
		player setVariable["surrender",false,true];
		player setVariable ["Escorting",false,true];
		player setVariable ["transporting",false,true];
		detach _player;
	};
	if(!alive _cop) exitWith {
	player setVariable ["Escorting",false,true];
	detach player;
	};
	
	if(vehicle player != player) then
	{
		//disableUserInput true;
		if(driver (vehicle player) == player) then {player action["eject",vehicle player];};
	}
		else
	{
		//disableUserInput false;
	};
};

//disableUserInput false;
		
if(alive player) then
{
life_restrained = false;
//SYNC STATES
[7] call SOCK_fnc_updatePartial;
player switchMove "AmovPercMstpSlowWrflDnon_SaluteIn";
player setVariable ["Escorting",false,true];
player setVariable ["transporting",false,true];
detach player;
};