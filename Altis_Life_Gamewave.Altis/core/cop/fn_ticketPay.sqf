/*
	File: fn_ticketPay.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Pays the ticket.
*/
if(isnil {life_ticket_val} OR isNil {life_ticket_cop}) exitWith {};
if(life_flouze < life_ticket_val) exitWith
{
	if(life_dabflouze < life_ticket_val) exitWith 
	{
		hint "Pas assez d'argent dans votre banque pour payer l'amende.";
		[[1,format["%1 est completement ruin� et ne peux pas payer votre amende.",name player]],"life_fnc_broadcast",life_ticket_cop,false] spawn life_fnc_MP;
		closeDialog 0;
	};
	hint format["Vous avez pay� l'amende d'un montant de %1�",[life_ticket_val] call life_fnc_numberText];
	life_dabflouze = life_dabflouze - life_ticket_val;
	life_ticket_paid = true;
	[[0,format["%1 a pay� une amende d'un montant de %2�",name player,[life_ticket_val] call life_fnc_numberText]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
    [[1,format["%1 a pay� la contravention.",profileName]],"life_fnc_broadcast",life_ticket_cop,false] spawn life_fnc_MP;
    [[life_ticket_val,player,life_ticket_cop],"life_fnc_ticketPaid",life_ticket_cop,false] spawn life_fnc_MP;

	[[getPlayerUID player],"life_fnc_wantedRemove",false,false] spawn life_fnc_MP;
	

	closeDialog 0;
};

life_flouze = life_flouze - life_ticket_val;
life_ticket_paid = true;

[[getPlayerUID player],"life_fnc_wantedRemove",false,false] spawn life_fnc_MP;
[[0,format["%1 a pay� une amende d'un montant de %2�",name player,[life_ticket_val] call life_fnc_numberText]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
closeDialog 0;
[[1,format["%1 a pay� l'amende.",name player]],"life_fnc_broadcast",life_ticket_cop,false] spawn life_fnc_MP;
[[life_ticket_val,player,life_ticket_cop],"life_fnc_ticketPaid",life_ticket_cop,false] spawn life_fnc_MP;

