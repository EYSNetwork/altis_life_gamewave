private["_b","_doors","_door"];
_b = _this select 0;
_doors = 1;
_doors = getNumber(configFile >> "CfgVehicles" >> (typeOf _b) >> "NumberOfDoors");

_door = 0;
//Find the nearest door
for "_i" from 1 to _doors do {
	_selPos = _b selectionPosition format["Door_%1_trigger",_i];
	_worldSpace = _b modelToWorld _selPos;
		if(player distance _worldSpace < 5) exitWith {_door = _i;};
};
if(_door == 0) exitWith {hint "Vous n'êtes pas près d'une porte..."};
 //Not near a door to be broken into.
 if((_b getVariable[format["bis_disabled_Door_%1",_door],0]) == 1)then {
_b setVariable[format["bis_disabled_Door_%1",_door],0,true];
}else {
_b setVariable[format["bis_disabled_Door_%1",_door],1,true];
};
closeDialog 0;