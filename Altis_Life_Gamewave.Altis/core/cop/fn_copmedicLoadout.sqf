/*
	File: fn_copLoadout.sqf
	Author: Bryan "Tonic" Boardwine
	Edited: Itsyuka
	
	Description:
	Loads the cops out with the default gear.
*/
private["_handle"];
_handle = [] spawn life_fnc_stripDownPlayer;
waitUntil {scriptDone _handle};
player setVariable["imMedicBro",true,true];
//Load player with default cop gear.
player addUniform "U_B_CTRG_1";
player addVest "V_TacVest_blk_POLICE";
player addHeadgear "H_Cap_police";
player addBackpack "B_FieldPack_blk";

player addWeapon "hgun_Pistol_heavy_02_Yorris_F";
player addMagazine "6Rnd_45ACP_Cylinder";
player addMagazine "6Rnd_45ACP_Cylinder";

player addWeapon "srifle_EBR_F";
player addPrimaryWeaponItem "muzzle_snds_B";
player addPrimaryWeaponItem "acc_flashlight";
player addPrimaryWeaponItem "optic_MRCO";

player addItemToBackpack "Medikit";
player addItemToBackpack "Toolkit";
player addItemToBackpack "FirstAidKit";
player addItemToBackpack "20Rnd_762x51_Mag";
player addItemToBackpack "20Rnd_762x51_Mag";
player addItemToBackpack "20Rnd_762x51_Mag";
player addItemToBackpack "20Rnd_762x51_Mag";
player addItemToBackpack "MiniGrenade";

/* ITEMS */
player addItem "ItemMap";
player addItem "NVGoggles";
player assignItem "NVGoggles";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemWatch";
player assignItem "ItemWatch";
player addItem "ItemGPS";
player assignItem "ItemGPS";

//[[player,0,"texture\skins\medic_uniform.jpg"],"life_fnc_setTexture",true,false] spawn life_fnc_MP;

//[] call life_fnc_saveGear;


