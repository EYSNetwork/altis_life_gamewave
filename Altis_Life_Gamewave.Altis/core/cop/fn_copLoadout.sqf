/*
	File: fn_copLoadout.sqf
	Author: Bryan "Tonic" Boardwine
	Edited: Itsyuka
	
	Description:
	Loads the cops out with the default gear.
*/
private["_handle"];
_handle = [] spawn life_fnc_stripDownPlayer;
waitUntil {scriptDone _handle};

//Load player with default cop gear.
player addUniform "U_Rangemaster";
player addVest "V_TacVest_blk_POLICE";
player addHeadgear "H_Cap_police";

player addWeapon "hgun_Pistol_heavy_02_Yorris_F";
player addMagazine "6Rnd_45ACP_Cylinder";
player addMagazine "6Rnd_45ACP_Cylinder";
player addMagazine "6Rnd_45ACP_Cylinder";
player addMagazine "6Rnd_45ACP_Cylinder";
player addMagazine "6Rnd_45ACP_Cylinder";
player addMagazine "6Rnd_45ACP_Cylinder";

/* ITEMS */
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemWatch";
player assignItem "ItemWatch";
player addItem "ItemGPS";
player assignItem "ItemGPS";

[] call life_fnc_saveGear;


