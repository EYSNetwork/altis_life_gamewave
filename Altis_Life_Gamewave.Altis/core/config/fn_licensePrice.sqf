/*
	File: fn_licensePrice.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Returns the license price.
*/
private["_type"];
_type = [_this,0,"",[""]] call BIS_fnc_param;
if(_type == "") exitWith {-1};

switch (_type) do
{
   case "driver": {5000}; //Cout de permis de conduire
   case "truck": {100000}; // Cout de la licence de camion
   case "boat": {1000}; //Permis de bateau
   case "pilot": {550000}; //Pilot/air license cost
   
   case "dive": {2000}; //Cout de la licence de plong�e
   case "cair": {15000}; // Permit pilote police
   case "swat": {35000}; //Swat License cost
   case "cg": {8000}; //Coast guard license cost
   case "medmarijuana": {1500}; // Marijuana medicale cout de la licence
   case "gang": {10000}; //Cout de la licence de Gang
   
   case "diamond": {35000}; // Licence transformation Diamant
   case "salt": {12000}; // Licence transformation sel
   case "sand": {14500}; // Licence transformation sable
   case "iron": {16500}; // Licence transformation fer
   case "copper": {8000}; // Licence transformation cuivre
   case "cement": {6500}; // Licence transformation ciment
   case "houblon": {9500}; //Cout de la license IV
   case "oil": {10000}; //Cout de permis de transformation p�trole
   
   case "home": {250000}; // License Houses
      
   case "gun": {45000}; //Cout de la licence d'arme � feu
   case "rebel": {1000000}; // Cout de la licence Rebel
   
   case "cocaine": {200000}; // Licence transformation cocaine
   case "marijuana": {200000}; // transformation de la marijuana
   case "heroin": {200000}; //Co�t de permis de transformation d'h�ro�ne
   case "meth": {200000}; // License Meth
   
   
   case "dep": {75000}; // Licence transformation ciment
   case "medic": {85000}; // Licence transformation ciment
   case "taxi": {55000}; // License Taxidufion
   case "merc": {1000000}; // License Taxidufion

};