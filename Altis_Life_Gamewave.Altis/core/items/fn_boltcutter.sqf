/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Breaks the lock on a single door (Closet door to the player).
*/
private["_building","_door","_doors","_cpRate","_title","_progressBar","_titleText","_cp","_ui","_sfx","_sfxplay","_jaildome"];
if((player getVariable "restrained")) exitWith {hint "Vous êtes attaché !";closeDialog 0;closeDialog 0;};
if((player getVariable "Surrender")) exitWith {hint "Vous n'avez pas les mains libres !";closeDialog 0;closeDialog 0;};
_building = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
_sfx = true;
_sfxplay = 0;
_jaildome = false;

if(isNull _building) exitWith {};
if(!(_building isKindOf "House_F")) exitWith {hint "Vous devez effectuer cette action sur une porte."};
if(isNil "life_boltcutter_uses") then {life_boltcutter_uses = 0;};

if(((nearestObject [[16019.5,16952.9,0],"Land_Dome_Big_F"]) != _building && (nearestObject [[16019.5,16952.9,0],"Land_Research_house_V1_F"]) != _building && _building != dome_jail)) exitWith {hint "La disqueuse ne marche que sur les portes de la banque ou de la prison !"};
if({side _x == west} count playableUnits < 4) exitWith {hint "Il est nécessaire qu'il y est au moins 5 policiers en ligne !"};

if((nearestObject [[16019.5,16952.9,0],"Land_Dome_Big_F"]) == _building OR (nearestObject [[16019.5,16952.9,0],"Land_Research_house_V1_F"]) == _building) then {
	[[[1,2],"!!!!! BRAQUAGE DE LA BANQUE FEDERALE !!!!!!"],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
	_jaildome = false;
}; 

if(_building == dome_jail) then {
_jaildome = true;
	[[[1,2],"!!!!! LA PRISON EST ATTAQUE !!!!!!"],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
	cop_jail_spawn = false;
	publicVariable "cop_jail_spawn";
}; 

//else {
//exitWith {hint "La disqueuse ne marche que sur la porte de la banque !"};
	//[[0,format["%1 was seen breaking into a house.",profileName]],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
//};

_doors = getNumber(configFile >> "CfgVehicles" >> (typeOf _building) >> "NumberOfDoors");


_door = 0;
//Find the nearest door
for "_i" from 1 to _doors do {
	_selPos = _building selectionPosition format["Door_%1_trigger",_i];
	_worldSpace = _building modelToWorld _selPos;
		if(player distance _worldSpace < 5) exitWith {_door = _i;};
};
if(_door == 0) exitWith {hint "Vous n'êtes pas proche d'une porte !"}; //Not near a door to be broken into.
if((_building getVariable[format["bis_disabled_Door_%1",_door],0]) == 0) exitWith {hint "Cette porte est déjà ouverte !"};
life_action_inUse = true;

//Setup the progress bar
disableSerialization;
_title = "Découpe de la porte";
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNamespace getVariable "life_progress";
_progressBar = _ui displayCtrl 38201;
_titleText = _ui displayCtrl 38202;
_titleText ctrlSetText format["%2 (1%1)...","%",_title];
_progressBar progressSetPosition 0.01;
_cP = 0.01;

switch (typeOf _building) do {
	case "Land_Dome_Small_F": {_cpRate = 0.0015;};
	case "Land_Dome_Big_F": {_cpRate = 0.003;};
	case "Land_Research_house_V1_F": {_cpRate = 0.0015;};
	default {_cpRate = 0.08;}
};

//[[player, "meuleuseIn",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;
//[[player, "meuleuseLoop",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;

while {true} do
{
	diag_log format ["_sfxplay %1", _sfxplay];
	diag_log format ["_sfx %1", _sfx];
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	if((_sfx)) then {
		[[player, "meuleuseLucel",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;
		_sfx = false;
	};
	
	sleep 0.2;
	
	_sfxplay = _sfxplay + 1;
	
	if(_sfxplay >= 100)then{
		_sfx = true;
		_sfxplay = 0;
	};
	if(isNull _ui) then {
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNamespace getVariable "life_progress";
		_progressBar = _ui displayCtrl 38201;
		_titleText = _ui displayCtrl 38202;
	};
	_cP = _cP + _cpRate;
	_progressBar progressSetPosition _cP;
	_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
	if(_cP >= 1 OR !alive player) exitWith {


	};
	if(life_istazed) exitWith {


	}; //Tazed
	if(life_interrupted) exitWith {
	

	
	};
};
//[[player, "meuleuseOut",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;

//Kill the UI display and check for various states
5 cutText ["","PLAIN"];
//[[player, "",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;
player playActionNow "stop";
if(!alive player OR life_istazed) exitWith {life_action_inUse = false;};
if((player getVariable["restrained",false])) exitWith {life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; titleText["Action cancelled","PLAIN"]; life_action_inUse = false;};
life_boltcutter_uses = life_boltcutter_uses + 1;
life_action_inUse = false;


if(life_boltcutter_uses >= 5) then {
	[false,"boltcutter",1] call life_fnc_handleInv;
	life_boltcutter_uses = 0;
};

_building setVariable[format["bis_disabled_Door_%1",_door],0,true]; //Unlock the door.
if((_building getVariable["locked",false])) then {
	_building setVariable["locked",false,true];
};
if(!_jaildome) then{
[[getPlayerUID player,profileName,"459"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
}else{
cop_jail_spawn = true;
publicVariable "cop_jail_spawn";
[[getPlayerUID player,profileName,"460"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
};