/*

Scan C4 sur veh

*/
private["_curTarget","_ui","_title","_progressBar","_cP","_titleText","_vehicle","_trackerList"];
//life_interrupted = false;
if((player getVariable "restrained")) exitWith {hint "Vous êtes attaché !";closeDialog 0;closeDialog 0;};
if((player getVariable "Surrender")) exitWith {hint "Vous n'avez pas les mains libres !";closeDialog 0;closeDialog 0;};
_curTarget = cursorTarget;
if(isNull _curTarget) exitWith {};
_vehicle = _curTarget;
_isVehicle = if((_curTarget isKindOf "landVehicle") OR (_curTarget isKindOf "Ship") OR (_curTarget isKindOf "Air")) then {true} else {false};
if(isNull _vehicle) exitWith {};
if(!_isVehicle) exitWith {};

_trackerList = _vehicle getVariable "vehicle_info_c4";

life_action_inUse = true;
	_upp = "Analyse du véhicule...";
	[[player, "scanner_lucel",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;
	//Setup our progress bar.
	disableSerialization;
	5 cutRsc ["life_progress","PLAIN"];
	_ui = uiNameSpace getVariable "life_progress";
	_progress = _ui displayCtrl 38201;
	_pgText = _ui displayCtrl 38202;
	_pgText ctrlSetText format["%2 (1%1)...","%",_upp];
	_progress progressSetPosition 0.01;
	_cP = 0.2;
	_check = true;
	while{true} do
	{
		sleep 0.25;
		_cP = _cP + 0.01;
		_progress progressSetPosition _cP;
		_pgText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_upp];
		if(_cP >= 1) exitWith {};
		if(player distance _vehicle > 10) exitWith {};
		if(!alive player) exitWith {};
		if((count crew _vehicle) > 0) exitWith {_check = false};
		//if(life_interrupted) exitWith {};
	};
	5 cutText ["","PLAIN"];
	if(!alive player) exitWith {life_action_inUse = false;};
	
	if(!_check) exitWith {
	[[4,"<t size='3'><t color='#FF0000'>Interruption du scanner.</t></t> <br/><t size='1.5'>Vous ne devez pas interagir avec le véhicule pendant le scanner.</t>"],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
	life_action_inUse = false;
	};	
	//if(life_interrupted) exitWith {life_interrupted = false; titleText["Action annulé","PLAIN"]; life_action_inUse = false;};
	if(player != vehicle player) exitWith {titleText["Vous devez être en dehors du véhicule pour le scanner","PLAIN"];};
		
		if((count _trackerList > 0) && _check) then 
			{
					[[player, "trackerfound_lucel",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;
					[[4,"<t size='1.5'><t color='#FF0000'>Véhicule piégé !</t></t> <br/><t size='1'>Une bombe est active sur ce véhicule. Utiliser un kit de désamorsage pour désactiver la charge explosive.</t>"],"life_fnc_broadcast",player,false] spawn life_fnc_MP;	
					_vehicle setVariable["c4detected",true];
			}
			else
			{
					[[4,"<t size='1.5'><t color='#00FF00'>Aucun résultat.</t></t><br/><t size='1'>Aucune anomalie détectée sur ce véhicule.</t>"],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
			};
	life_action_inUse = false;		