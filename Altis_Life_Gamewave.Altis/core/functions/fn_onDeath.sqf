/*
	File: fn_onDeath.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Does whatever it needs to when a player dies.
*/
private["_unit","_killer","_weapons","_handle","_namekiller","_namekilled","_type","_uidKiller","_uidKilled"];
_unit = [_this,0,Objnull,[Objnull]] call BIS_fnc_param;
_source = [_this,1,Objnull,[Objnull]] call BIS_fnc_param;
_namekiller = name _source;
_namekilled =  name _unit;
_type = "";
_uidKiller = getPlayerUID _source;
_uidKilled = getPlayerUID _unit;

if(isNull _unit) exitWith {};
_unit setVariable["Revive",FALSE,TRUE]; //Set the corpse to a revivable state.
_unit setVariable["name",profileName,TRUE]; //Set my name so they can say my name.
_unit setVariable["restrained",FALSE,TRUE];
_unit setVariable["Escorting",FALSE,TRUE];
_unit setVariable["transporting",FALSE,TRUE]; //Why the fuck do I have this? Is it used?
_unit setVariable["steam64id",(getPlayerUID player),true]; //Set the UID.

cutText["Attente de la réapparition...","BLACK FADED"];
0 cutFadeOut 9999999;

if(playerSide == civilian) then
{
	removeAllContainers _unit;
};
if(playerSide == west) then
{
	life_dabflouze = life_dabflouze - 40000;
	hint "Vous avez perdu 40 000€ pour votre mort...";
};
_unit setVariable ["FAR_isUnconscious", 0, true];
_unit setVariable ["FAR_isDragged", 0, true];
hideBody _unit;
//Make my killer wanted!
if(side _source != west && alive _source) then
{
	if(vehicle _source isKindOf "LandVehicle") then
	{
		if(alive _source) then
		{
			[[getPlayerUID _source,name _source,"187V"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
			_type = "CarKill";

		};
	}
		else
	{
		[[getPlayerUID _source,name _source,"187"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
		_type = "Mort par balle";

	};
};

if(side _source == west && !life_use_atm) then
{
	if(life_flouze != 0) then
	{
		[format["%1€ sont de retours à la banque suite a un braquage raté.",[life_flouze] call life_fnc_numberText],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
		life_flouze = 0;
	};
};

//New addition for idiots.
if(side _source == civilian && _source != _unit && !local _source) then
{
	if(vehicle _source isKindOf "LandVehicle") then {
		[[2],"life_fnc_removeLicenses",_source,false] spawn life_fnc_MP;
		_type = "Carkill";
	} else {
		//[[3],"life_fnc_removeLicenses",_source,false] spawn life_fnc_MP;
	};
};

if(side _source == west && vehicle _source == _source && playerSide == civilian) then
{
	[[player,_source,true],"life_fnc_wantedBounty",false,false] spawn life_fnc_MP;
	[[getPlayerUID player],"life_fnc_wantedRemove",false,false] spawn life_fnc_MP;
	//[[getPlayerUID player],"life_fnc_wantedPunish",false,false] spawn life_fnc_MP;
}
	else
{
	if(playerSide == civilian) then
	{
		[[getPlayerUID _unit],"life_fnc_wantedRemove",false,false] spawn life_fnc_MP;
	};
};

//[_unit,_source] call DB_fnc_logDeath;
[[_namekilled,_namekiller,_type,_uidKiller,_uidKilled],"DB_fnc_logDeath",false,false] spawn life_fnc_MP;

_handle = [_unit] spawn life_fnc_dropItems;
waitUntil {scriptDone _handle};

switch (playerSide) do
	{
		case civilian: 
		{
			if(((getPos player) distance (getMarkerPos "reb_spawn_1")) < 1000) then {life_reb_respawn_1 = false;[] spawn {sleep (5 * 60);life_reb_respawn_1 = true;};};
			if(((getPos player) distance (getMarkerPos "reb_spawn_2")) < 1000) then {life_reb_respawn_2 = false;[] spawn {sleep (5 * 60);life_reb_respawn_2 = true;};};
			if(((getPos player) distance (getMarkerPos "reb_spawn_3")) < 1000) then {life_reb_respawn_3 = false;[] spawn {sleep (5 * 60);life_reb_respawn_3 = true;};};
			if(((getPos player) distance (getMarkerPos "reb_spawn_4")) < 1000) then {life_reb_respawn_4 = false;[] spawn {sleep (5 * 60);life_reb_respawn_4 = true;};};		
			if(((getPos player) distance (getMarkerPos "reb_spawn_5")) < 1000) then {life_reb_respawn_5 = false;[] spawn {sleep (5 * 60);life_reb_respawn_5 = true;};};		
			if(((getPos player) distance (getMarkerPos "civ_spawn_1")) < 1000) then {life_civ_respawn_1 = false;[] spawn {sleep (5 * 60);life_civ_respawn_1 = true;};};
			if(((getPos player) distance (getMarkerPos "civ_spawn_2")) < 1000) then {life_civ_respawn_2 = false;[] spawn {sleep (5 * 60);life_civ_respawn_2 = true;};};
			if(((getPos player) distance (getMarkerPos "civ_spawn_3")) < 1000) then {life_civ_respawn_3 = false;[] spawn {sleep (5 * 60);life_civ_respawn_3 = true;};};
			if(((getPos player) distance (getMarkerPos "civ_spawn_4")) < 1000) then {life_civ_respawn_4 = false;[] spawn {sleep (5 * 60);life_civ_respawn_4 = true;};};
			if(((getPos player) distance (getMarkerPos "civ_spawn_5")) < 1000) then {life_civ_respawn_5 = false;[] spawn {sleep (5 * 60);life_civ_respawn_5 = true;};};
		};
	};


//life_death_loc = (getpos _unit);
life_carryWeight = 0;
life_thirst = 100;
life_hunger = 100;
life_damage = 100;
life_use_atm = true;
life_flouze = 0;
life_alive = false;
life_in_rea = false;
life_restrained = false;
life_bidet = [0,0,0];

[] call SOCK_fnc_updateRequest;
