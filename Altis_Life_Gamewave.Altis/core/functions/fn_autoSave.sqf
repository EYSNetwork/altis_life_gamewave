/*
    @Version: 0.2
    @Author: =IFD= Cryptonat
    @Edited: 4/4/14
    
    Description:
    Saves the player's gear every 10 minutes.
*/

while {true} do {
	sleep (800 + (random 350));
    if (alive player) then {
        if (playerside == west) then {
		[7] call SOCK_fnc_updatePartial;
		}
		else 
		{
		[] call SOCK_fnc_updateRequest;
		};
    };
}; 

