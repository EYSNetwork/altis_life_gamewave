/*
	File: fn_ticketPay.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Pays the ticket.
*/

if(isnil {life_taxi_ticket_val} OR isNil {life_ticket_taxi}) exitWith {};

if(life_flouze < life_taxi_ticket_val) exitWith
{
	if(life_dabflouze < life_taxi_ticket_val) exitWith
	{	
		//Client
		hint parseText format["<t size='3'><t color='#FF0000'>Facture :</t></t> <br/><t size='1.5'>Pas assez d'argent dans votre compte en banque bancaire pour payer la facture du chauffeur de taxi.</t>"];
		systemChat format["Vous n'avez pas assez d'argent sur votre compte en banque pour payer la facture."];
		//taxianneur
		[[4,format["<t size='3'><t color='#FF0000'>Facture :</t></t> <br/><t size='1.5'>%1 est completement ruiné, il n'a plus rien sur son compte en banque et donc ne peut pas payer votre facture...</t>",name player]],"life_fnc_broadcast",life_ticket_taxi,false] spawn life_fnc_MP;
		[[0,format["%1 ne peut pas payer votre facture.",name player]],"life_fnc_broadcast",life_ticket_taxi,false] spawn life_fnc_MP;
		//COP
		[[0,format["[Activité Civil] %1 a refuser le réglement d'une facture (%2€) demandé par le chauffeur de taxi %3",name player,[life_taxi_ticket_val] call life_fnc_numberText,life_ticket_taxi]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
		closeDialog 0;
		//Ajout WantedList
		[[getPlayerUID player,name player,"700"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
		
	};
	life_dabflouze = life_dabflouze - life_taxi_ticket_val;
	life_taxi_ticket_paid = true;
	//TO CLIENT
	hint parseText format["<t size='3'><t color='#00FF00'>Facture :</t></t> <br/><t size='1.5'>Vous avez payé une facture d'un montant de %1€, avec l'argent de votre compte bancaire.</t>",[life_taxi_ticket_val] call life_fnc_numberText];
	systemChat format["Facture payé avec l'argent de votre compte bancaire"];
	//TO taxi
	[[4,format["<t size='3'><t color='#00FF00'>Facture :</t></t> <br/><t size='1.5'>%1 à payé votre facture. L'argent est directement viré sur votre compte bancaire. Vous avez gagné :<t color='#00FF00'> %2€</t></t>",name player,[life_taxi_ticket_val] call life_fnc_numberText]],"life_fnc_broadcast",life_ticket_taxi,false] spawn life_fnc_MP;
	[[0,format["%1 a payé la facture.",name player]],"life_fnc_broadcast",life_ticket_taxi,false] spawn life_fnc_MP;
	//TO COP
	[[0,format["[Activité Civil] %1 a payé une facture(%2€) au chauffeur de taxi %3",name player,[life_taxi_ticket_val] call life_fnc_numberText,life_ticket_taxi]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
	//PAIEMENT VIA COMPTE
    [[life_taxi_ticket_val,player,life_ticket_taxi],"life_fnc_taxi_ticketPaid",life_ticket_taxi,false] spawn life_fnc_MP;
	//Livré de compte ?
	//[[getPlayerUID player],"life_fnc_wantedRemove",false,false] spawn life_fnc_MP;
	closeDialog 0;
};

life_flouze = life_flouze - life_taxi_ticket_val;
life_taxi_ticket_paid = true;
//Livré de compte ? historique ?
//[[getPlayerUID player],"life_fnc_wantedRemove",false,false] spawn life_fnc_MP;
closeDialog 0;

//TO CLIENT
hint parseText format["<t size='3'><t color='#00FF00'>Facture :</t></t> <br/><t size='1.5'>Vous avez payé une facture d'un montant de %1€, facture déduit directement de votre porte-monnaie.</t>",[life_taxi_ticket_val] call life_fnc_numberText];
systemChat format["Facture payé avec l'argent de votre porte-monnaie."];

//TO taxi
[[0,format["%1 a payé la facture en liquide. Vous avez gagné %2€",name player,[life_taxi_ticket_val] call life_fnc_numberText]],"life_fnc_broadcast",life_ticket_taxi,false] spawn life_fnc_MP;
[[4,format["<t size='3'><t color='#00FF00'>Facture :</t></t> <br/><t size='1.5'>%1 à payé votre facture en liquide. L'argent est directement viré sur votre compte bancaire. Vous avez gagné :<t color='#00FF00'> %2€</t></t>",name player,[life_taxi_ticket_val] call life_fnc_numberText]],"life_fnc_broadcast",life_ticket_taxi,false] spawn life_fnc_MP;

//TO COP
[[0,format["[Activité Civil] %1 a payé une facture(%2€) au chauffeur de taxi %3",name player,[life_taxi_ticket_val] call life_fnc_numberText,life_ticket_taxi]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;

//PAIEMENT VIA COMPTE
[[life_taxi_ticket_val,player,life_ticket_taxi],"life_fnc_taxi_ticketPaid",life_ticket_taxi,false] spawn life_fnc_MP;

