/*
	File: fn_pickupItem.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master handling for picking up an item.
	LUCEL WAS HERE !!! 
	RESPECTEZ-VOUS !!!
*/
private["_obj","_itemInfo","_itemName","_illegal","_diff"];
if((time - life_action_delay) < 2) exitWith {hint "Tu ne peux pas faire ça aussi vite !"};
_obj = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _obj OR isPlayer _obj) exitWith {};
if((_obj getVariable["PickedUp",false])) exitWith {hint "Objet en cours d'utilisation par une autre personne."}; //Object was already picked up.
if(player distance _obj > 3) exitWith {};
//_itemInfo = _obj getVariable "item";

_varItems = _obj getVariable "item" select 0;
_valItems = _obj getVariable "item" select 1;
if(count _varItems <= 0) exitWith {deleteVehicle _obj;}; //Object was already picked up.
_var = _varItems select 0;
_val = _valItems select 0;
_itemInfo = [_var,_val];

_itemName = [([_itemInfo select 0,0] call life_fnc_varHandle)] call life_fnc_varToStr;
_illegal = [_itemInfo select 0,life_illegal_items] call TON_fnc_index;

if(playerSide == west && _illegal != -1) exitWith
{
	titleText[format["%1 a été mis dans le sac à preuves, tu as reçu $%2 de prime !",_itemName,[(life_illegal_items select _illegal) select 1] call life_fnc_numberText],"PLAIN"];
	life_dabflouze = life_dabflouze + ((life_illegal_items select _illegal) select 1);
	deleteVehicle _obj;
	waitUntil {isNull _obj};
	life_action_delay = time;
};

life_action_delay = time;

_diff = [_itemInfo select 0,_itemInfo select 1,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
//diag_log format["pickup _diff %1",_diff];
if(_diff <= 0) exitWith {hint "Tes poches sont pleines !"};
_obj setVariable["PickedUp",TRUE,TRUE];

if(_diff != _itemInfo select 1) then
{
	if(([true,_itemInfo select 0,_diff] call life_fnc_handleInv)) then
	{
		player playmove "AinvPknlMstpSlayWrflDnon";
		sleep 0.5;
		_valItems set [0,((_itemInfo select 1) -_diff)];
		_obj setVariable["item",[_varItems,_valItems],true];
		diag_log format["pickup _diff %1",_diff];
        titleText[format["Vous avez ramassé %1 %2",_diff,_itemName],"PLAIN"];
		_obj setVariable["PickedUp",false,true];
	};
}
	else
{
	if(([true,_itemInfo select 0,_itemInfo select 1] call life_fnc_handleInv)) then
	{
		//deleteVehicle _obj;
		//waitUntil{isNull _obj};
		_varItems deleteAt 0;
		_valItems deleteAt 0;	
		_obj setVariable["item",[_varItems,_valItems],true];
		player playmove "AinvPknlMstpSlayWrflDnon";
		sleep 0.5;
        titleText[format["Vous avez ramassé %1 %2",_diff,_itemName],"PLAIN"];
		_obj setVariable["PickedUp",false,true];
		if(count _varItems <= 0) exitWith {deleteVehicle _obj;}; 
	};
};