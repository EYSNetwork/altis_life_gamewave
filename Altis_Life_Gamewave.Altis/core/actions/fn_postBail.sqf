/*
	File: fn_postBail.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Called when the player attempts to post bail.
	Needs to be revised.
*/
private["_unit"];
_unit = _this select 1;
if(life_bail_paid) exitWith {};
if(isNil {life_bail_amount}) then {life_bail_amount = 50000;};
if(!isNil "life_canpay_bail") exitWith {hint "Tu dois attendre 3 Minutes avant de payer ta caution !"};
if(life_dabflouze < life_bail_amount) exitWith {hint format["Tu n'as pas %1€ sur ton compte pour payer ta caution truand !",life_bail_amount];};

life_dabflouze = life_dabflouze - life_bail_amount;
life_bail_paid = true;
[[0,format["%1 vient de payer sa caution", name _unit]],"life_fnc_broadcast",true,false] spawn life_fnc_MP;