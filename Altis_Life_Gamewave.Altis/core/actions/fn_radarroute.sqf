// Radar en ville
// Par Georges Monfrere pour Altislife.fr / Gamewave.

_speed = speed player;
_veh = vehicle player;

if((_veh getVariable "SirenMedic")) exitWith {};
if((_veh getVariable "SirenCop")) exitWith {};


if((_speed > 130) && (_speed < 199)) then
{
hint "Vous avez été flashé !!! La limitation est de 120km/h !";
cutText["Vous avez été flashé !","WHITE IN"];
[[getPlayerUID player,name player,"802"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
};

if((_speed > 200) && (_speed < 299)) then
{
hint "Vous avez été flashé ! La limitation est de 120km/h !";
cutText["Vous avez été flashé !","WHITE IN"];
[[getPlayerUID player,name player,"803"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
};

if (_speed > 300) then
{
Hint "RADAR ERROR CALCULATING SPEED ! SPEED TOO HIGH..."
cutText["Vous avez été flashé !","WHITE IN"];
};