#include <macro.h>
/*
	File: fn_unimpound.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Yeah... Gets the vehicle from the garage.
*/
if((time - life_action_delay) < 10) exitWith {hint "Attendez 10 secondes avant de sortir un nouveau véhicule..."};
life_action_delay = time;
private["_vehicle","_vid","_pid","_unit","_price"];

disableSerialization;
if(lbCurSel 2802 == -1) exitWith {hint localize "STR_Global_NoSelection"};
_vehicle = lbData[2802,(lbCurSel 2802)];
_vehicle = (call compile format["%1",_vehicle]) select 0;

_vid = lbValue[2802,(lbCurSel 2802)];
_pid = getPlayerUID player;
_unit = player;


if(isNil "_vehicle") exitWith {hint "Il y a une erreur dans la sélection..."};
//diag_log format["unimpound: %1",_vehicle];
//diag_log format["unimpound: %1",_vid];
//diag_log format["unimpound: %1",_pid];
//diag_log format["unimpound: %1",_unit];

_price = [_vehicle,__GETC__(life_garage_prices)] call TON_fnc_index;
if(_price == -1) then {_price = 1000;} else {_price = (__GETC__(life_garage_prices) select _price) select 1;};
if(life_dabflouze < _price) exitWith {hint format[(localize "STR_Garage_CashError"),[_price] call life_fnc_numberText];};

if(typeName life_garage_sp == "ARRAY") then {
			[[_vid,_pid,life_garage_sp select 0,_unit,_price,life_garage_sp select 1],"TON_fnc_spawnVehicle",false,false] spawn life_fnc_MP;
			diag_log format["LAUNCH SPAWN VEH"];
} else {
		if(life_garage_sp in ["medic_spawn_1","medic_spawn_2","medic_spawn_3"]) then {
				[[_vid,_pid,life_garage_sp,_unit,_price],"TON_fnc_spawnVehicle",false,false] spawn life_fnc_MP;
				diag_log format["LAUNCH SPAWN VEH MEDICDUFION"];
		} else {

				[[_vid,_pid,(getMarkerPos life_garage_sp),_unit,_price,markerDir life_garage_sp],"TON_fnc_spawnVehicle",false,false] spawn life_fnc_MP;
				diag_log format["LAUNCH SPAWN VEH2"];
				};
		};		


/*
[[_vid,_pid,(getMarkerPos life_garage_sp),_unit,_price],"TON_fnc_spawnVehicle",false,false] spawn life_fnc_MP;
*/
hint localize "STR_Garage_SpawningVeh";
life_dabflouze = life_dabflouze - _price;
closedialog 0;