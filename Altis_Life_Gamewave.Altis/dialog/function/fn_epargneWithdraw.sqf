/*
	COPY PASTE TIME
*/
if(playerSide == west) exitWith {hint "Vous ne pouvez pas retirer cette argent en tant que Policier ! C'est pour votre retraite ! Pensez a vos petits enfants !"};
if((time - life_action_delay) < 2) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_val"];
_val = parseNumber(ctrlText 2702);
if(_val > 999999) exitWith {hint "Attention vous ne pouvez pas dépasser le plafond de retrait : 999 999 €";};
if(_val < 0) exitwith {};
if(!([str(_val)] call life_fnc_isnumeric)) exitWith {hint "Je ne suis qu'un distributeur..."};
if(_val > life_epargne) exitWith {hint "Retrait refusé."};
if(_val < 100 && life_epargne > 20000000) exitWith {hint "Désolé mais votre banque n'autorise que les retraits de plus de 100€."}; //Temp fix for something.

life_flouze = life_flouze + _val;
life_epargne = life_epargne - _val;
hint format ["Vous avez retirer %1€ de votre compte d'épargne",[_val] call life_fnc_numberText];
[] call life_fnc_epargneMenu;
[] call SOCK_fnc_updateRequest; //Silent Sync