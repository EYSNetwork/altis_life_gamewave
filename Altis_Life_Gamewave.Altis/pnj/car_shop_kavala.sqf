/* 
	CarshopLucel
*/
_this enableSimulation false; 
_this allowDamage false; 

_this addAction["<t color='#AAF200'>Concessionnaire Civil</t>",
life_fnc_vehicleShopMenu,["civ_car",civilian,["civ_car_1","civ_car_1_1"]
,"civ","Concessionnaire Automobile"]];

_this addAction["<t color='#AAF200'>Concessionnaire Taxi</t>",
life_fnc_vehicleShopMenu,["taxi_shop",civilian,["civ_car_1","civ_car_1_1"]
,"taxi","Concessionnaire Automobile"],90,false,false,"",'playerSide == civilian && license_civ_taxi'];  

_this addAction["<t color='#AAF200'>Concessionnaire Dépanneur</t>",
life_fnc_vehicleShopMenu,["dep_shop",civilian,["civ_car_1","civ_car_1_1"]
,"dep","Concessionnaire Automobile"],90,false,false,"",'playerSide == civilian && license_civ_dep'];  

_this addAction["<t color='#AAF200'>Concessionnaire Médecin</t>",
life_fnc_vehicleShopMenu,["med_shop",civilian,["civ_car_1","civ_car_1_1"]
,"medic","Concessionnaire Automobile"],90,false,false,"",'playerSide == civilian && license_civ_medic'];  



_this addAction["<t color='#FF9900'>Garage</t>",
{  [[getPlayerUID player,playerSide,"Car",player],"TON_fnc_getVehicles",false,false] spawn life_fnc_MP;
life_garage_type = "Car";
createDialog "Life_impound_menu";
disableSerialization;
ctrlSetText[2802,"Recherches des véhicules...."];
life_garage_sp = "civ_car_1_1";  }];
_this addAction["<t color='#FF9900'>Rentrer au garage</t>",life_fnc_storeVehicle,"",0,false,false,"",'!life_garage_store'];
_this setVariable["realname", "Concessionnaire Automobile"];
